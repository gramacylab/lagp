## This file is to improve laGP's performance on irregular grids. The first step 
## is to find a method to capture global information in a comprehensive way (better than global sub GP) and 
## then using local GP for (local) lengthscale estimation and subsequent (local) prediction. 

# load training data
# load("../../data/sattemp/SatelliteTemps.RData")  ## real data
load("../../data/sattemp/SimulatedTemps.RData")  ## simulated data

data <- sim.data
# class(data)
# [1] "data.frame"
# dim(data)
# [1] 150000      3
nr <- 500 # number of rows
nc <- 300 # number of columns
# manipulate data frame: longitude, latitude, and temperature
Lon <- matrix(data$Lon, nrow=nr, ncol=nc)
Lat <- matrix(data$Lat, nrow=nr, ncol=nc)
Temp <- matrix(data$Temp, nrow=nr, ncol=nc)

# extract the indices of the missing values
nas <- which(is.na(data$Temp))
X <- cbind(data$Lon, data$Lat)[-nas,] # training input
N <- nrow(X) # size of training data
# N
# [1] 105569
y <- data$Temp[-nas] # training response
XX <- cbind(data$Lon, data$Lat)[nas,] # testing input
NN <- nrow(XX) # size of testing data
# NN
# [1] 44431

# scale training and testing input to a hyper unit cube
maxX <- apply(rbind(X, XX), 2, max)
minX <- apply(rbind(X, XX), 2, min)
for(j in 1:ncol(X)){
    X[,j] <- X[,j] - minX[j]	
    X[,j] <- X[,j]/(maxX[j]-minX[j])  
    
    XX[,j] <- XX[,j] - minX[j]	
    XX[,j] <- XX[,j]/(maxX[j]-minX[j])
}

## 1. GLOBAL first
############################## global subset GP ##############################
library(tgp)
library(laGP)
library(plgp)

n <- 100 # size of subset to be used to fit global GP
sub <- dopt.gp(n, Xcand=X) # the subset: a D-optimal design of size n

## priors for the global (subset) GP 
da <- darg(list(mle=TRUE, max=10), X, samp.size=10000) # 10000 might be the largest sample size which is computationally tractable in a common laptop
ga <- garg(list(mle=TRUE, max=10), y)

## fit the global subset GP
gpsepi <- newGPsep(sub$XX, y[sub$fi], d=da$start, g=ga$start, dK=TRUE)
that <- mleGPsep(gpsepi, param="both", tmin=c(da$min, ga$min), 
                 tmax=c(da$max, ga$max), ab=c(da$ab, ga$ab), maxit=200)

## predictions using the fitted global subset GP on the testing set
psub <- predGPsep(gpsepi, XX, lite=TRUE)

## calculation of residuals at the training input locations
pX <- predGPsep(gpsepi, X, lite=TRUE)
yresid <- y - pX$mean

## done with the global subset GP, clean up
deleteGPsep(gpsepi)

############################## multiresolution approximation (MRA) ##############################
devtools::install_github("katzfuss-group/GPvecchia")
library(GPvecchia)

############################## periodic embedding (PE) ##############################
devtools::install_github("joeguinness/aldodevel")
devtools::install_github("joeguinness/npspec")
library(aldodevel)
library(npspec)

# read in daytime land surface temperature data and put in a matrix
tmpr <- matrix(data$Temp, nr, nc)

# get grid size
nvec_obs <- c(nr, nc)

# get pattern of missing values
ype <- tmpr[1:nvec_obs[1],1:nvec_obs[2]] # responses
observed <- !is.na(ype) # non-missing responses
nobs <- sum(observed)   # number of non-missing responses

# define locations and covariates
locs <- as.matrix(expand.grid(1:nvec_obs[1], 1:nvec_obs[2]))
# dim(locs)
# [1] 150000      2
Xpe <- array(NA, c(nvec_obs, 3))
Xpe[,,1] <- 1
Xpe[,,2] <- array(locs[,1], nvec_obs)
Xpe[,,3] <- array(locs[,2], nvec_obs)

# fit the data with periodic embedding
t1 <- proc.time()[3]
fit <- iterate_spec(ype, observed, X=Xpe, burn_iters=30, 
                    par_spec_fun=spec_AR1, embed_fac=1.2,
                    precond_method="Vecchia", m=10, silent=TRUE, ncondsim=50)
t2 <- proc.time()[3]
pe.time <- (t2 - t1)/60
pe.time
# elapsed 
# ~11 mins

# predictive means
pred_mat <- fit$condexp
pred_vec <- c(pred_mat)

# figure out in-sample and out-of-sample predictive means
psub_pe <- cbind(data$Temp, pred_vec)
psub_pe_insample <- psub_pe[-nas,]
# dim(psub_pe_insample)
# [1] 105569    2
psub_pe_outsample <- psub_pe[nas,]
# dim(psub_pe_outsample)
# [1] 44431     2

# calculate the predictive variances based on the conditional simulations
cond_diff <- array(NA, dim(fit$condsim))
for(j in 1:dim(fit$condsim)[3]){
    cond_diff[,,j] <- fit$condsim[,,j] - fit$condexp
}
meansq <- function(x){
    1/length(x) * sum(x^2)
}
## predictive variances
predvar_mat <- apply(cond_diff, c(1,2), meansq)
predvar_vec <- c(predvar_mat)

# visualization
par(mfrow=c(1,2))
fields::image.plot(pred_mat)
fields::image.plot(predvar_mat)

## using local GP to fit the residual from in-sample prediction
yresid_pe <- psub_pe_insample[,1] - psub_pe_insample[,2]
out_pe <- aGPsep(X, yresid_pe, XX, d=list(start=1, max=20), g=1e-6, omp.threads=7, verb=1)

## combined predictive means
mean_pe <- out_pe$mean + psub_pe_outsample[,2]
## combined predictive variances
var_pe <- out_pe$var + predvar_vec # the second part is incorrect!!!

################################## PE: for the full data set ##################################
# read in daytime land surface temperature data and put in a matrix
load("../../data/sattemp/AllSatelliteTemps.RData")
nr <- 500
nc <- 300
tmpr <- matrix(all.sat.temps$MaskTemp, nr, nc)
nas <- which(is.na(all.sat.temps$MaskTemp))

X <- cbind(all.sat.temps$Lon, all.sat.temps$Lat)[-nas,] # training input
XX <- cbind(all.sat.temps$Lon, all.sat.temps$Lat)[nas,] # testing input

# scale training and testing input to a hyper unit cube
maxX <- apply(rbind(X, XX), 2, max)
minX <- apply(rbind(X, XX), 2, min)
for(j in 1:ncol(X)){
    X[,j] <- X[,j] - minX[j]	
    X[,j] <- X[,j]/(maxX[j] - minX[j])  
  
    XX[,j] <- XX[,j] - minX[j]	
    XX[,j] <- XX[,j]/(maxX[j] - minX[j])
}

# get grid size
nvec_obs <- c(nr, nc)

# get pattern of missing values
ype <- tmpr[1:nvec_obs[1],1:nvec_obs[2]]
observed <- !is.na(ype)
nobs <- sum(observed)

# define locations and covariates
locs <- as.matrix(expand.grid(1:nvec_obs[1], 1:nvec_obs[2]))
Xpe <- array(NA, c(nvec_obs, 3))
Xpe[,,1] <- 1
Xpe[,,2] <- array(locs[,1], nvec_obs)
Xpe[,,3] <- array(locs[,2], nvec_obs)

# fit the data with periodic embedding
fit <- iterate_spec(ype, observed, X = Xpe, burn_iters = 20, par_spec_fun = spec_AR1, 
                    embed_fac = 1.2, precond_method = "Vecchia", m = 10, silent = TRUE, ncondsim = 50)

# predictive means
pred_mat <- fit$condexp
pred_vec <- c(pred_mat)

# figure out in-sample and out-of-sample predictive means
psub_pe <- cbind(all.sat.temps$MaskTemp, pred_vec)
psub_pe_insample <- psub_pe[-nas,]
# dim(psub_pe_insample)
# [1] 105569    2
psub_pe_outsample <- psub_pe[nas,]
# dim(psub_pe_outsample)
# [1] 44431     2

# calculate the predictive variances based on the
# conditional simulations
cond_diff <- array(NA, dim(fit$condsim))
for(j in 1:dim(fit$condsim)[3]){
    cond_diff[,,j] <- fit$condsim[,,j] - fit$condexp
}
meansq <- function(x){
    1/length(x)*sum(x^2)
}
# predictive variances
predvar_mat <- apply(cond_diff, c(1,2), meansq)
predvar_vec <- c(predvar_mat)

# local GP on the residual---try to capture local information in a more comprehensive way
yresid_pe <- psub_pe_insample[,1] - psub_pe_insample[,2]
# range(yresid_pe)
# [1] -1.623148e-06  3.464777e-06
out_pe <- aGPsep(X, yresid_pe, XX, d=list(start=1, max=20), g=1e-05, omp.threads=7, verb=1)
# range(out_pe$mean)
# [1] -2.161720e-06  3.466975e-06

# combined predictive means
mean_pl <- out_pe$mean + psub_pe_outsample[,2]
psub_pe[nas,2] <- mean_pl

##################### predictive accuracy #####################
# rmse and mae for PE only
npred <- sum(is.na(all.sat.temps$MaskTemp)) - sum(is.na(all.sat.temps$TrueTemp)) # 42740
rmse_pe <- sqrt(sum((pred_vec - all.sat.temps$TrueTemp)^2, na.rm = TRUE)/npred)
mae_pe <- sum(abs(pred_vec - all.sat.temps$TrueTemp), na.rm = TRUE)/npred
rmse_pe
# 1.77782
mae_pe
# 1.277959

# rmse and mae for (PE + local GP)
rmse_pl <- sqrt(sum((psub_pe[,2] - all.sat.temps$TrueTemp)^2, na.rm = TRUE)/npred)
mae_pl <- sum(abs(psub_pe[,2] - all.sat.temps$TrueTemp), na.rm = TRUE)/npred
rmse_pl
# 1.77782
mae_pl
# 1.277959

## 2. local lengthscale estimation and subsequent local prediction using laGP
############ Now for laGP on residuals from the global subset GP ############
## scale the inputs according to the macro-analysis lengthscales
scale <- sqrt(that$theta[1:2]) # take the square root of the estimated lengthscale
# scale both the training and testing input
Xs <- X; XXs <- XX
for(j in 1:ncol(Xs)){
    Xs[,j] <- Xs[,j] / scale[j]
    XXs[,j] <- XXs[,j] / scale[j]
}

## local prediction on residuals
out <- aGPsep(Xs, yresid, XXs, d=list(start=1, max=20), g=that$theta[3], omp.threads=7, verb=1) # do not use all the cores

## predicted mean
mean_lagp <- psub$mean + out$mean

## predicted variance (should remove the nugget estimate from psub's variance; psub: predictions using the fitted global subset GP on the testing set)
K <- covar.sep(sub$XX, d=that$theta[1:2], g=that$theta[3])
psi <- drop(t(y[sub$fi]) %*% solve(K) %*% y[sub$fi])
s2.f <- psub$s2 * nrow(sub$XX) / psi
eps <- sqrt(.Machine$double.eps) # for purpose of numerical stability
s2.f <- s2.f - that$theta[3] + eps
s2.f <- s2.f * psi / nrow(sub$XX)
var_lagp <- s2.f + out$var

## visualization
par(mfrow=c(1,2))
# mean
p2 <- data$Temp
p2[nas] <- mean_lagp
PTemp2 <- matrix(p2, nrow=nr, ncol=nc)
image.plot(Lon, Lat, PTemp2, 
           zlim=range(data$Temp, na.rm=TRUE),
           xlab="Longitude", ylab="Latitude",
           main="Global-Local laGP")
stdev <- sqrt(var_lagp)
# standard deviation
p3 <- data$Temp
p3[nas] <- stdev
p3[-nas] <- NA
PTemp3 <- matrix(p3, nrow=nr, ncol=nc)
image.plot(Lon, Lat, PTemp3, 
           xlab="Longitude", ylab="Latitude",
           main="Predictive SD based on Global-Local laGP")
