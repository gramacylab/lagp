library(laGP)
library(boot)

f2d <- function(x, y=NULL)
  {
    if(is.null(y)) {
      if(!is.matrix(x)) x <- matrix(x, ncol=2)
      y <- x[,2]; x <- x[,1]
    }
    g <- function(z)
      return(exp(-(z-1)^2) + exp(-0.8*(z+1)^2) - 0.05*sin(8*(z+0.1)))
    z <- -g(x)*g(y)
  }

## build up a design with N=~40K locations
x <- seq(-2, 2, by=0.02)
X <- as.matrix(expand.grid(x, x))
Xeval <- X
Xeval[,1] <- 4*(inv.logit(2*Xeval[,1])-0.5)
Z <- f2d(Xeval)

## args shared by all
nth <- 8
method <- "alcray"
verb <- 0

## predictive grid with NN=400 locations,
## change NN to 10K (length=100) to mimic setup in Gramacy & Apley (2014)
## recommend NN >= 100 (length=10) for pretty results below;
## the low NN set here is for fast CRAN checks
xx <- seq(-1.975, 1.975, length=100)
XX <- as.matrix(expand.grid(xx, xx))
XXeval <- XX
XXeval[,1] <- 4*(inv.logit(2*XXeval[,1])-0.5)
ZZ <- f2d(XXeval)

## out <- aGP(X, Z, XX, method=method, omp.threads=nth, verb=verb)
## cat("iso RMSE", sqrt(mean((out$mean - ZZ)^2)), "\n")

parlaGPsep <- laGPsep## .R
## formals(parlaGPsep)$parallel <- "omp"
out.sep <- aGPsep.R(X, Z, XX, method=method, laGPsep=parlaGPsep, verb=verb)
cat("sep RMSE", sqrt(mean((out.sep$mean - ZZ)^2)), "\n")

sub <- sample(1:nrow(X), 1000, replace=FALSE)
gpsepi <- newGPsep(X[sub,], Z[sub], out$d$start, g=1/1000, dK=TRUE)
mle <- mleGPsep(gpsepi, param="d", tmin=out$d$min, tmax=out$d$max, ab=out$d$ab)
psep <- predGPsep(gpsepi, XX, lite=TRUE)
deleteGPsep(gpsepi)

cat("global subset sep RMSE", sqrt(mean((psep$mean - ZZ)^2)), "\n");

scales <- sqrt(mle$d)
Xs <- X; XXs <- XX
for(j in 1:ncol(Xs)) {
	Xs[,j] <- Xs[,j]/scales[j]
	XXs[,j] <- XXs[,j]/scales[j]
}

outs <- aGP(XX=XXs, X=Xs, Z=Z, method=method, d=list(start=1), g=1/1000, 
		omp.threads=nth, verb=verb)
cat("scaled iso RMSE", sqrt(mean((outs$mean - ZZ)^2)), "\n")
outs.sep <- aGPsep.R(XX=XXs, X=Xs, Z=Z, method=method, d=list(start=1), 
    g=1/1000, laGPsep=parlaGPsep, verb=verb)
cat("scaled sep RMSE", sqrt(mean((outs.sep$mean - ZZ)^2)), "\n")

## second round

df <- data.frame(y=log(out$mle$d), XX)
lo <- loess(y~., data=df, span=0.01)
dfs <- data.frame(y=log(outs$mle$d), XX)
los <- loess(y~., data=dfs, span=0.01)
df1 <- data.frame(y=log(out.sep$mle[,1]), XX)
lo1 <- loess(y~., data=df1, span=0.01)
df2 <- data.frame(y=log(out.sep$mle[,2]), XX)
lo2 <- loess(y~., data=df2, span=0.01)
dfs1 <- data.frame(y=log(outs.sep$mle[,1]), XX)
los1 <- loess(y~., data=dfs1, span=0.01)
dfs2 <- data.frame(y=log(outs.sep$mle[,2]), XX)
los2 <- loess(y~., data=dfs2, span=0.01)

par(mfrow=c(1,2))
image(xx, xx, matrix(lo1$fitted, nrow=length(xx)), 
	col=heat.colors(128), main="sep 1")
image(xx, xx, matrix(lo2$fitted, nrow=length(xx)), 
	col=heat.colors(128), main="sep 2")

out2 <- aGP(X, Z, XX, method=method, d=exp(lo$fitted), omp.threads=nth, verb=verb)
cat("round 2, iso RMSE", sqrt(mean((out2$mean - ZZ)^2)), "\n")
out2.sep <- aGPsep.R(X, Z, XX, method=method, laGPsep=parlaGPsep, verb=verb,
		d=cbind(exp(lo1$fitted), exp(lo2$fitted)))
cat("round 2, sep RMSE", sqrt(mean((out2.sep$mean - ZZ)^2)), "\n")
out2s <- aGP(XX=XXs, X=Xs, Z=Z, method=method, d=exp(los$fitted), g=1/1000, 
		omp.threads=nth, verb=verb)
cat("rount 2, scaled iso RMSE", sqrt(mean((out2s$mean - ZZ)^2)), "\n")
out2s.sep <- aGPsep.R(XX=XXs, X=Xs, Z=Z, method=method, laGPsep=parlaGPsep, 
		d=cbind(exp(los1$fitted), exp(los2$fitted)), verb=verb)
cat("round 2, scaled sep RMSE", sqrt(mean((out2s.sep$mean - ZZ)^2)), "\n")


df <- data.frame(y=log(out2$mle$d), XX)
lo <- loess(y~., data=df, span=0.01)
df1 <- data.frame(y=log(out2.sep$mle[,1]), XX)
lo1 <- loess(y~., data=df1, span=0.01)
df2 <- data.frame(y=log(out2.sep$mle[,2]), XX)
lo2 <- loess(y~., data=df2, span=0.01)

image(xx, xx, matrix(lo1$fitted, nrow=length(xx)), 
	col=heat.colors(128), main="sep 1 it 2")
image(xx, xx, matrix(lo2$fitted, nrow=length(xx)), 
	col=heat.colors(128), main="sep 2 it 2")


## checking the correlation between successive iterations
par(mfrow=c(2,2))
plot(log(out.sep$mle[,1]), log(out2.sep$mle[,1]), main="sep 1")
plot(log(out.sep$mle[,2]), log(out2.sep$mle[,2]), main="sep 2")
plot(log(out$mle$d), log(out2$mle$d))
