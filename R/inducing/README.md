# Repository for Locally Induced Gaussian Processes
# Maintainer: Austin Cole

	austin.cole8@vt.edu

## Includes:	
- **code**: directory with files sources to run experiments
- **data**: directory with data used in or generated from some experiments
- **experiments**: directory with scripts to run experiments in paper
- **figure_scripts**: scripts to generate figures in paper. Some require data generated from experiments.
- **src**: C-code for function used to build nearest neighbor neighborhoods
