## Experiments performed in the paper, "Locally induced Gaussian processes for large-scale simulation experiments"
### D. Austin Cole, Ryan Christianson, Robert B. Gramacy

## Includes:	
- **Big_Sim_BH.R**: simulation comparing the root mean squared error from liGP Borehole predictions on different neighborhood sizes and number of inducing points Described in Appendix B.
- **Big_Sim_HT.R**: simulation comparing the root mean squared error from liGP Herbie's Tooth predictions on different nieghborhood sizes and number of inducing points. Described in Appendix B.
- **borehole_experiment.R**: simulation of the borehole function comparing predictions from various liGP template schemes with various forms of laGP. Described in Section 5.2.
- **rmse_bakeoff_vfe_alc_imse.R**: comparison of rooth mean squared error predictions from liGP with inducing points sequentially selected using VFE, ALC, and IMSE. Described in Section 2.3.
- **sarcos_experiment.R**: experiment using the SARCOS data, comparing predictions from various liGP template schemes with various forms of laGP. Described in Section 5.3.
- **sat_drag_experiment.R**: experiment using the Satellite Drag data from the repository 'tpm'. Compares predictions from various liGP template schemes with various forms of laGP. Described in Section 5.4. 
