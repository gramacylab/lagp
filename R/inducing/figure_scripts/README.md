## Scripts to produce figures in the paper, "Locally induced Gaussian processes for large-scale simulation experiments"
### D. Austin Cole, Ryan Christianson, Robert B. Gramacy

## Includes:	
- **big_nm_sim_figures**: figure 10
- **borehole_figures.R**: figure 6
- **compare_opt_ip_designs_to_templates_figure.R**: figure 4
- **global_ip_opt_surface_figures.R**: figure 9
- **herbie_tooth_slice_prediction_error_figures.R**: figure 3
- **lhs_scaled_templates_for_borehole_in_2d_figures.R**: figure 5
- **rmse_bakeoff_vfe_alc_imse_figure.R**: figure 1
- **sarcos_figure.R**: figure 7
- **sat_drag_figures.R**: figure 8
- **seq_ip_selection_with_wimse_figures.R**: figure 2
