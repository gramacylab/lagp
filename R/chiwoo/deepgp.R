
data <- read.csv("non_noisy_data.csv",  header=FALSE)
X <- data[,1:2]
Y <- data[,3]

library(laGP)
glob <- newGPsep(X, Y, d=0.1, g=1e-6, dK=TRUE)
mle <- mleGPsep(glob)
xx <- seq(-0.5, 0.5, length=200)
XX <- expand.grid(xx, xx)
pglob <- predGPsep(glob, XX, lite=TRUE)

## visuals
cs <- heat.colors(128)
bs <- seq(-25, 25, length=129)
par(mfrow=c(1,2))
image(xx, xx, matrix(pglob$mean, ncol=length(xx)), col=cs, breaks=bs, 
    xlab="x1", ylab="x2", main="Ordinary, Global GP fit")
hist(pglob$mean, freq=FALSE, breaks=40, xlim=c(-25, 30), ylim=c(0, 0.075),
    xlab="output, y", main="marginal Y: train and predict")
hist(Y, freq=FALSE, add=TRUE, breaks=40, border=2)
legend("topright", c("global pred", "train"), fill=1:2)

library(deepgp)
dgp <- fit_two_layer(as.matrix(X), Y/20, vecchia=TRUE)  ## got nu=2.5 -> 1.5 warning
## might be a good idea to define print s3 method for dgp* object(s)
dgpt <- trim(dgp, 1000, 10)
p <- predict(dgpt, as.matrix(XX)) ## Error says predictive locations are too close together