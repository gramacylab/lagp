## This directory contains examples of *laGP* in action in several contexts.

### Below lists what each sub-directory comprises of:

- `boosting`: Adam Edward's work on an inverse-weighted approach to local kriging based on a patchwork of fits

- `cokriging`: exploratory work on GP modeling for multiple outputs 

- `gpu`: early tests on *Graphical Processing Unit* extensions to *laGP*.  These have since been incorporated into laGP as *alcgpu*, with illustrations in our JUQ paper

- `misc`: some examples that have arisen in response to queries from users, including the sarcos data and a bake-off paper (submitted to JASA) on predicting temperature measured via satellite

- `optim`: test code for optimization under constraints via augmented Lagrangian and expected improvement, supporting publications in Technometrics and at NIPS.  These examples have mostly been incorporated into the *laGP* documentation

- `solance`: synthesizing simulation and field data of solar irradiance; collaborators: Benjamin Haaland (University of Utah), Siyuan Lu (IBM Thomas J. Watson Research Center) and Youngdeok Hwang (Sungkyunkwan University).  Note that this content has been moved to new project `solance-hg/R`.  Soon this will be removed.

- `calib`: examples from AoAS paper on modularized calibration with a large corpous of computer model calibration; some of these have been incorporated into the *laGP* vignette/JSS paper and the examples in documentation

- `early`: tests of very early versions of *laGP*, leading to the initial JCGS publication; most of the usage tested in these files are now deprecated, and updated versions have been incorporated into the *laGP* vignette/JSS paper and the examples in the documentation

- `lgbb`: examples that went into the JUQ GPU paper using the Langley Glide-back Booster

- `noisy`: initial experiments on local estimation of nuggets; these are the basis of a few examples in the vignette/JSS paper

- `rays`: code supporting cheaper search via ray (or line) searches, illustrated in the vignette/JSS paper and supporting a Technometrics publication

### This directory used to contain the following sub-directories which have been moved to new repositories.

- `tpm`: examples for the test particle Monte Carlo estimation of satellite drag coefficients; now in `gramacylab/tpm`
