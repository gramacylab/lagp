require(MASS)
source("gp.R")

X <- matrix(mcycle[,1], ncol=1)
## X <- (X + min(X))
## X <- X/max(X)
Z <- mcycle[,2]
## Z <- Z/sd(Z)
XX <- matrix(seq(min(X), max(X), length=100), ncol=1)

out <- approxGP(X=X, Z=Z, XX=XX, end=30, 
				  g=list(mle=TRUE), verb=1) 


par(mfrow=c(2,1))
df <- data.frame(y=log(out$mle$d), XX)
lo <- loess(y~., data=df, span=0.25)
plot(XX, log(out$mle$d), type="l")
lines(XX, lo$fitted, col=2)
dfnug <- data.frame(y=log(out$mle$g), XX)
lonug <- loess(y~., data=dfnug, span=0.25)
plot(XX, log(out$mle$g), type="l")
lines(XX, lonug$fitted, col=2)

out2 <- approxGP(X=X, Z=Z, XX=XX, end=30, 
				   d=list(start=exp(lo$fitted), mle=FALSE),
				   g=list(start=exp(lonug$fitted)))

par(mfrow=c(1,1))
plot(X,Z)
df <- 20
s2 <- out2$var*(df-2)/df
q1 <- qt(0.05, df)*sqrt(s2) + out2$mean
q2 <- qt(0.95, df)*sqrt(s2) + out2$mean
lines(XX, out2$mean)
lines(XX, q1, col=1, lty=2)
lines(XX, q2, col=1, lty=2)

stop()

## debugging
out.loc <- localGP.R(XX[71,,drop=FALSE], 6, 20, X, Z, g=100, verb=1)

g <- garg(list(mle=TRUE), Z)

gp <- newGP(X[out.loc$Xi,,drop=FALSE], Z[out.loc$Xi], d=out.loc$mle$d, g=100, dK=TRUE)

## stop()
## mleGP(gp, param="d", tmin=d$min, tmax=d$max, ab=c(d$a, d$b), verb=2)
## mleGP(gp, param="g", tmin=out.loc$garg$min, tmax=out.loc$garg$max, ab=c(out.loc$garg$a, out.loc$garg$b), verb=2)

jmleGP(gp, drange=c(out.loc$d$min, out.loc$d$max), grange=c(out.loc$g$min, out.loc$g$max), 
	   dab=out.loc$d$ab, gab=out.loc$g$ab)

dg <- data.frame(d=out.loc$mle$d, g=seq(1,1000,length=1000))
## ll <- llikGP.d(dg, X=X, y=Z, d$ab, param="d")
ll <- llikGP.d(dg, gpi.in=gp, ab=g$ab, param="g")
par(mfrow=c(3,1))
plot(dg$g, ll$llik, type="l")
plot(dg$g, ll$d, type="l")
plot(dg$g, ll$d2, type="l")