library(laGP,) ## lib.loc="~/Rlibs/")

## read in the tgp-expand lgbb "grid" with 3 inputs and 6 outputs
source("lgbb.R")

## a snow version
## library(snow)
library(Rmpi)
library(parallel)

## set up cluster
np <- 4 ## mpi.universe.size()
nth <- 4
## nodes <- system("scontrol show hostnames", intern=TRUE)
## print(nodes)
## make the snow cluster
cl <- makeCluster(np, type="MPI")
print(length(cl))

## run parameters
g <- 1/100000
nn <- nrow(XX) 

out <- aGP.parallel(cl, XX=XX[1:nn,], X=X, Z=Z, g=g, method="nn", verb=0, 
	omp.threads=16, end=128)
d <- out$mle$d; d[d > 0.9*out$d$max] <- 0.9*out$d$max
out2 <- aGP.parallel(cl, XX=XX[1:nn,], X=X, Z=Z, g=g, d=d, verb=0, omp.threads=nth, close=2000, end=128)
## out2 <- aGP.parallel(cl, XX=XX[1:nn,], X=X, Z=Z, g=g, d=d, verb=0, num.gpus=2, gpu.threads=16, 
	## omp.threads=13, nn.gpu=round(0.9*nn), close=2000, end=128)
##	omp.threads=10, nn.gpu=round(0.8*nn), close=10000)#, end=128)

## nn <- 10000
## out2.noGP <- aGP(XX=XX[1:nn,], X=X, Z=Z, g=g, d=d, omp.threads=16, end=128, close=2000)

## shut down the cluster and save the output
stopCluster(cl)
save(out, out2, file=paste("lgbb_snow_", length(cl), "_c2_e128.RData", sep=""))
##save(out, out2, file=paste("lgbb_snow_", length(cl), "x2gpu_c2_e128.RData", sep=""))
## save(out, out2, file=paste("lgbb_snow_", length(cl), "x2gpu_c10-2.RData", sep=""))
## save(out, out2, file=paste("lgbb_", length(cl), "x2gpu_c10.RData", sep=""))

mpi.exit()