library(lhs)
library(laGP)
source("tricands.R")

## used lots below
eps <- sqrt(.Machine$double.eps)


##
## bakeoff code
##

## f:
##
## Goldstein-Price test function

f <- function(X)
 {
  if(is.null(nrow(X))) X <- matrix(X, nrow=1)
  m <- 8.6928
  s <- 2.4269
  x1 <- 4*X[,1] - 2
  x2 <- 4*X[,2] - 2
  a <- 1 + (x1 + x2 + 1)^2 * 
    (19 - 14*x1 + 3*x1^2 - 14*x2 + 6*x1*x2 + 3*x2^2)
  b <- 30 + (2*x1 - 3*x2)^2 * 
    (18 - 32*x1 + 12*x1^2 + 48*x2 - 36*x1*x2 + 27*x2^2)
  f <- log(a*b)
  f <- (f - m)/s
  return(f)
 }


## EIt:
##
## Identical to EI below (Chapter 7 of Surroages) with
## a unique counter

tricnt <- 0
EIt <- function(gpi, x, fmin, pred=predGPsep)
 {
  if(is.null(nrow(x))) x <- matrix(x, nrow=1)
  tricnt <<- tricnt + nrow(x)
  p <- pred(gpi, x, lite=TRUE)
  d <- fmin - p$mean
  sigma <- sqrt(p$s2)
  dn <- d/sigma
  ei <- d*pnorm(dn) + sigma*dnorm(dn)
  return(ei)
 }


## EI:
##
## Identical to EI from Chapter 7 of Surrogates except
## with additional counter

cnt <- 0
EI <- function(gpi, x, fmin, pred=predGPsep)
 {
  if(is.null(nrow(x))) x <- matrix(x, nrow=1)
  cnt <<- cnt + nrow(x)
  p <- pred(gpi, x, lite=TRUE)
  d <- fmin - p$mean
  sigma <- sqrt(p$s2)
  dn <- d/sigma
  ei <- d*pnorm(dn) + sigma*dnorm(dn)
  return(ei)
 }


## bov:
##
## Best Observed Value metric from Chapter 7 of Surrogates

bov <- function(y, end=length(y))
 {
  prog <- rep(min(y), end)
  prog[1:min(end, length(y))] <- y[1:min(end, length(y))]
  for(i in 2:end) 
    if(is.na(prog[i]) || prog[i] > prog[i-1]) prog[i] <- prog[i-1]
  return(prog)
 }



## EI.tri:
## 
## new EI fucntion that evaluates EI on triangulated 
## gap-filling candidates

EI.tri <- function(X, y, gpi, pred=predGPsep)
 {
  m <- which.min(y)
  fmin <- y[m]
  Xcand <- tricands(X, max=50, best=m)
  solns <- data.frame(cbind(0, 0, Xcand, EIt(gpi, Xcand, fmin)))
  names(solns) <- c("s1", "s2", "x1", "x2", "val")
  return(solns)
}


## EI.search and obj.EI:
##
## Identical from Chapter 7 or Surrogates

obj.EI <- function(x, fmin, gpi, pred=predGPsep) 
  - EI(gpi, x, fmin, pred)

EI.search <- function(X, y, gpi, pred=predGPsep, multi.start=5, tol=eps)
 {
  m <- which.min(y)
  fmin <- y[m]
  start <- matrix(X[m,], nrow=1)
  if(multi.start > 1) 
    start <- rbind(start, randomLHS(multi.start - 1, ncol(X)))
  xnew <- matrix(NA, nrow=nrow(start), ncol=ncol(X)+1)
  for(i in 1:nrow(start)) {
    if(EI(gpi, start[i,], fmin) <= tol) { out <- list(value=-Inf); next }
    out <- optim(start[i,], obj.EI, method="L-BFGS-B", 
      lower=0, upper=1, gpi=gpi, pred=pred, fmin=fmin)
    xnew[i,] <- c(out$par, -out$value)
  }
  solns <- data.frame(cbind(start, xnew))
  names(solns) <- c("s1", "s2", "x1", "x2", "val")
  solns <- solns[solns$val > tol,]
  return(solns)
}


## optim.EI:
##
## borrowed from Chapter 7 of Surrogates, modified to 
## use custom initialization, and to allow tricands candidates

optim.EI <- function(f, ninit, end, tri=FALSE, init=NULL)
 {
  ## initialization
  if(is.null(init)) {
    X <- randomLHS(ninit, 2)
    y <- f(X)
  } else {  ## grab initial points from argument
    X <- init$X[1:ninit,]
    y <- init$y[1:ninit]
  }
  gpi <- newGPsep(X, y, d=0.1, g=1e-6, dK=TRUE)
  da <- darg(list(mle=TRUE, max=0.5), randomLHS(1000, 2))
  mleGPsep(gpi, param="d", tmin=da$min, tmax=da$max, ab=da$ab)
    
  ## optimization loop of sequential acquisitions
  maxei <- c()
  for(i in (ninit+1):end) {
    if(tri) solns <- EI.tri(X, y, gpi) 
    else solns <- EI.search(X, y, gpi)
    m <- which.max(solns$val)
    maxei <- c(maxei, solns$val[m])
    xnew <- as.matrix(solns[m,3:4])
    ynew <- f(xnew)
    updateGPsep(gpi, xnew, ynew)
    mleGPsep(gpi, param="d", tmin=da$min, tmax=da$max, ab=da$ab)
    X <- rbind(X, xnew)
    y <- c(y, ynew)
  }

  ## clean up and return
  deleteGPsep(gpi)
  return(list(X=X, y=y, maxei=maxei))
 }

##
## Monte Carlo EI experiment
##

reps <- 30
end <- 50
ninit <- 12
prog.eitri <- prog.ei <- matrix(NA, nrow=reps, ncol=end)
for(r in 1:reps) {
  os <- optim.EI(f, ninit, end)
  prog.ei[r,] <- bov(os$y)
  os <- optim.EI(f, ninit, end, tri=TRUE, init=list(X=os$X, y=os$y))
  prog.eitri[r,] <- bov(os$y)
}

##
## vis results 
##

## summarize Best-Observed-Value (BOV) progress
par(mfrow=c(1,2))
plot(colMeans(prog.ei), col=1, lwd=2, type="l", 
  xlab="n: blackbox evaluations", ylab="average best objective value")
lines(colMeans(prog.eitri), col=2, lwd=2)
abline(v=ninit, lty=2, col="gray")
legend("top", c("EI", "EI-tri", "seed LHS"), 
  col=c(1, 2, "gray"), lwd=c(2,2,1), lty=c(1,1,2), bty="n")

## distribution of final solution
boxplot(prog.ei[,end], prog.eitri[,end], ylim=range(c(prog.ei, prog.eitri)),
  names=c("EI", "EI-tri"), border=c("black", "red"), 
  xlab="comparator", ylab="best objective value")
text(x=c(1,2), y=c(1,1), labels=round(c(cnt/reps, tricnt/reps)))
text(x=1.5, y=1.5, labels="average EI evals")