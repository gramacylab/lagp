/* R CMD SHLIB -o predvar.so predvar.c */

#include <R_ext/Utils.h>
#include <R.h>
#include <Rmath.h>

char uplo = 'U';


void predvar_R(int *n_in, int *nn_in, double *ktKi_in, double *k_in, double *ktKik_out)
  {
  
    unsigned int i, j;

  	for(i=0; i<*nn_in; i++) {
  		ktKik_out[i] = 0.0;
  		for(j=0; j<*n_in; j++) ktKik_out[i] += ktKi_in[j*(*nn_in)+i]*k_in[j*(*nn_in) + i];
  	}

 }