source("functions.R")

par(mfcol = c(1,1))
x = runif(1000, max = 20) %>% sort
num = 10
amplitude = runif(num, max = 5)
period = runif(num, max = 5)
offset = runif(num, max = 2*pi)
y = rep(0, length(x))
for(i in 1:num){
  y = y + amplitude[i]*sin(period[i]*(x+offset[i]))
}
plot(x, y, type = 'l')
center = runif(1, max = 20)
sd = 0.01*(x-center)^2
y_obs = y+rnorm(1000, sd = sd)
points(x = x, y = y_obs, pch = "+", cex = .5)
plot(x, y, type = 'l')


xmods = list()
for(i in 1:39){
  temp = data.frame(x = x[(25*(i-1)+1):(25*i+25)], y = y_obs[(25*(i-1)+1):(25*i+25)])
  xmods[[i]] = lm(y~x + I(x^2) + I(x^3), data = temp)
  if(i == 1){
    cormat = matrix(1, ncol = 1, nrow = 1)
  }else{
    cormat = rbind(cormat, NA) %>% cbind(NA)
    for(j in 1:i){
      xtemp = data.frame(x = c(xmods[[i]]$model$x, xmods[[j]]$model$x))
      predsi = predict(xmods[[i]], newdata = xtemp)
      predsj = predict(xmods[[j]], newdata = xtemp)
      cormat[j,i] = cormat[i,j] = cor(predsi, predsj)
    }
  }
}
cormat2 = cormat
cormat[cormat<0] = 0
cormat2[cormat2<1] = 1

preds = lapply(xmods, function(z) predict(z, newdata = data.frame(x = x), se.fit = T))
preds_1 = lapply(preds, function(z) z$fit) %>% unlist %>% matrix(ncol = length(preds))
preds_2 = lapply(preds, function(z) z$se.fit) %>% unlist %>% matrix(ncol = length(preds))
preds_3 = lapply(xmods, function(z) sqrt(deviance(z)/df.residual(z))) %>% unlist

# for(i in 1:length(preds)){
#   col = sample(1:100, 1)
#   lines(x = x, y = preds[[i]]$fit, col = col)
#   lines(x = x, y = preds[[i]]$fit + 1.96*preds[[i]]$se.fit, col = col, lty = 2)
#   lines(x = x, y = preds[[i]]$fit - 1.96*preds[[i]]$se.fit, col = col, lty = 2)
#   Sys.sleep(2)
# }
lapply(preds, function(z) {col = sample(1:100, 1)
  lines(x = x, y = z$fit, col = col)
  lines(x = x, y = z$fit + 1.96*z$se.fit, col = col, lty = 2)
  lines(x = x, y = z$fit - 1.96*z$se.fit, col = col, lty = 2)
  })

weight = apply(1/(preds_2^2), 1, function(x) x/sum(x)) %>% t
apply(weight, 1, function(z) which(z == max(z))) %>% table
power = log(length(xmods), 2)
weight = apply(weight, 1, function(x) x^power/sum(x^power)) %>% t
pred_X = apply(preds_1*weight, 1, sum)
ws = preds_2*weight
ws_pred = t(apply(preds_2, 1, function(z) z+preds_3))*weight
se_X = apply(ws, 1, function(z) matrix(z, nrow = 1) %*% cormat %*% matrix(z, ncol = 1))
se_X2 = apply(ws, 1, function(z) matrix(z, nrow = 1) %*% cormat2 %*% matrix(z, ncol = 1))
se_pred = apply(ws_pred, 1, function(z) matrix(z, nrow = 1) %*% cormat %*% matrix(z, ncol = 1))
se_pred2 = apply(ws_pred, 1, function(z) matrix(z, nrow = 1) %*% cormat2 %*% matrix(z, ncol = 1))
par(mfcol = c(1,2))
plot(x, y, type = 'l', main = 'Mean Surface')
lines(x = x, y = pred_X, col = 'red')
lines(x = x, y = pred_X+1.96*sqrt(se_X), col = 'red', lty = 2)
lines(x = x, y = pred_X-1.96*sqrt(se_X), col = 'red', lty = 2)
lines(x = x, y = pred_X+1.96*sqrt(se_X2), col = 'blue', lty = 2)
lines(x = x, y = pred_X-1.96*sqrt(se_X2), col = 'blue', lty = 2)
abline(v = center, col = 'blue')
plot(x, y, type = 'l', main = "Predictive Surface")
lines(x = x, y = pred_X, col = 'red')
lines(x = x, y = pred_X+1.96*sqrt(se_pred), col = 'red', lty = 2)
lines(x = x, y = pred_X-1.96*sqrt(se_pred), col = 'red', lty = 2)
lines(x = x, y = pred_X+1.96*sqrt(se_pred2), col = 'green', lty = 2)
lines(x = x, y = pred_X-1.96*sqrt(se_pred2), col = 'green', lty = 2)
abline(v = center, col = 'blue')
points(x = x, y = y_obs, pch = "+", cex = 0.5)
par(mfcol = c(1,1))

plot(x = x, y = sd, ylab = "standard deviation", main = "Standard Deviation\nSurface", type = 'l',
     ylim = range(c(sqrt(se_pred), sd)))
abline(v = center, col = 'blue')
lines(x = x, y = sqrt(se_pred), col = 'red')
lines(x = x, y = sqrt(se_pred2), col = 'blue')


# twoord.plot(lx = x, ly = y, rx = x, ry = y > pred_X - 1.96*sqrt(se_X) & y < y + 1.96*sqrt(se_X),
#             type = 'l', main = 'Mean Surface', rcol = 'green')
# lines(x = x, y = pred_X, col = 'red')
# lines(x = x, y = pred_X+1.96*sqrt(se_X), col = 'red', lty = 2)
# lines(x = x, y = pred_X-1.96*sqrt(se_X), col = 'red', lty = 2)
# abline(v = center, col = 'blue')

sum(y > pred_X - 1.96*sqrt(se_X) & y < pred_X + 1.96*sqrt(se_X))/length(y)
sum(y > pred_X - 1.96*sqrt(se_X2) & y < pred_X + 1.96*sqrt(se_X2))/length(y)
sum(y_obs > pred_X - 1.96*sqrt(se_pred) & y_obs < pred_X + 1.96*sqrt(se_pred))/length(y)
sum(y_obs > pred_X - 1.96*sqrt(se_pred2) & y_obs < pred_X + 1.96*sqrt(se_pred2))/length(y)

ex_1d = list(pred = c(score(Y = y_obs, mu = pred_X, s2 = se_pred),
                      score(Y = y_obs, mu = pred_X, s2 = se_pred2)),
             mean = c(score(Y = y, mu = pred_X, s2 = se_X),
                      score(Y = y, mu = pred_X, s2 = se_X2)))


# dat = gramacy(n = 100, domain = default, sd = 0)
# names(dat) = c("x1", "x2", "y")
dat = f2d(n = 100, domain = default, sd = 0)
dat2 = f2d(n = 101, domain = default, sd = 0)
splits = seq(-2,2,length = 17)
# dat = michal(n = 100, m = 2, d = 2, sd = 0)
# splits = seq(0, pi, length = 17)
X = dat[,1:2]
center = runif(2, min = range(splits)[1], max = range(splits)[2])
y_obs = dat$y + rnorm(nrow(dat), sd = .01*distance(matrix(center, ncol = 2), X) %>% apply(2, sum))
y_pred = dat2$y + rnorm(nrow(dat2), sd = .01*distance(matrix(center, ncol = 2), dat2[,1:2]) %>% apply(2, sum))

dat_obs = cbind(X, y = y_obs)
dat_pred = cbind(dat2[,1:2], y_pred)

mods = list()
for(i in 1:(length(splits)-2)){
  for(j in 1:(length(splits)-2)){
    temp = filter(dat_obs, x1>= splits[i] & x1 <= splits[i+2]) %>%
      filter(x2>=splits[j] & x2 <= splits[j+2])
    mods[[paste("section", i, j, sep = "_")]] =
      lm(y~x1+x2+I(x1^2)+I(x2^2)+x1:x2, data = temp)
    if(i==j & i == 1){
      cormat = matrix(1, nrow = 1, ncol = 1)
    }else{
      cormat = rbind(cormat, NA) %>% cbind(NA)
      for(k in 1:length(mods)){
        xtemp = rbind(mods[[length(mods)]]$model, mods[[k]]$model)
        predsi = predict(mods[[length(mods)]], newdata = xtemp)
        predsj = predict(mods[[k]], newdata = xtemp)
        cormat[k,length(mods)] = cormat[length(mods),k] = cor(predsi, predsj)
      }
    }
  }
  print(i)
}
cormat2 = cormat
cormat[cormat<0] = 0
cormat2[cormat2<1] = 1

preds = lapply(mods, function(z) predict(z, newdata = dat2, se.fit = T))
preds_1 = lapply(preds, function(z) z$fit) %>% unlist %>% matrix(ncol = length(preds))
preds_2 = lapply(preds, function(z) z$se.fit) %>% unlist %>% matrix(ncol = length(preds))
preds_3 = lapply(mods, function(z) sqrt(deviance(z)/df.residual(z))) %>% unlist

weight = apply(1/(preds_2^2), 1, function(x) x/sum(x)) %>% t
apply(weight, 1, function(z) which(z == max(z))) %>% table
power = log(length(xmods), 2)
weight = apply(weight, 1, function(x) x^power/sum(x^power)) %>% t
pred_X = apply(preds_1*weight, 1, sum)
ws = preds_2*weight
ws_pred = t(apply(preds_2, 1, function(z) z+preds_3))*weight
se_X = apply(ws, 1, function(z) matrix(z, nrow = 1) %*% cormat %*% matrix(z, ncol = 1))
se_X2 = apply(ws, 1, function(z) matrix(z, nrow = 1) %*% cormat2 %*% matrix(z, ncol = 1))
se_pred = apply(ws_pred, 1, function(z) matrix(z, nrow = 1) %*% cormat %*% matrix(z, ncol = 1))
se_pred2 = apply(ws_pred, 1, function(z) matrix(z, nrow = 1) %*% cormat2 %*% matrix(z, ncol = 1))
breaks = seq(range(y_obs)[1], range(y_obs)[2], length = 131)
par(mfcol = c(2,2))
# image(x = X$x1 %>% unique, y = X$x2 %>% unique,
#       z = matrix(dat$y, ncol = sqrt(nrow(X))), col = heat.colors(130),
#       main = "Truth", xlab = "x1", ylab = "x2", breaks = breaks)
# image(x = X$x1 %>% unique, y = X$x2 %>% unique,
#       z = matrix(y_obs, ncol = sqrt(nrow(X))), col = heat.colors(130),
#       main = "Observed", xlab = "x1", ylab = "x2", breaks = breaks)
# # abline(h = splits, v = splits)
# image(x = X$x1 %>% unique, y = X$x2 %>% unique,
#       z = matrix(pred_X, ncol = sqrt(nrow(X))), col = heat.colors(130),
#       main = "Mean Surface", xlab = "x1", ylab = "x2", breaks = breaks)
# image(x = X$x1 %>% unique, y = X$x2 %>% unique,
#       z = matrix(se_X %>% sqrt, ncol = sqrt(nrow(X))), col = heat.colors(130),
#       main = "Standard Deviation Surface", xlab = "x1", ylab = "x2")
# points(x = center[1], y = center[2], pch = 20)
# points(x = center[1], y = center[2], col = 'white')
# se_range = range(se_X)
# mtext(paste("min: ", round(se_range[1], 4),
#             ", max: ", round(se_range[2], 4), sep = ""), side = 4)

image(x = X$x1 %>% unique, y = X$x2 %>% unique,
      z = matrix(dat$y, ncol = sqrt(nrow(X))), col = heat.colors(130),
      main = "Truth", xlab = "x1", ylab = "x2", breaks = breaks)
image(x = X$x1 %>% unique, y = X$x2 %>% unique,
      z = matrix(y_obs, ncol = sqrt(nrow(X))), col = heat.colors(130),
      main = "Observed", xlab = "x1", ylab = "x2", breaks = breaks)
# abline(h = splits, v = splits)
image(x = dat2$x1 %>% unique, y = dat2$x2 %>% unique,
      z = matrix(pred_X, ncol = sqrt(nrow(dat2))), col = heat.colors(130),
      main = "Mean Surface", xlab = "x1", ylab = "x2", breaks = breaks)
image(x = dat2$x1 %>% unique, y = dat2$x2 %>% unique,
      z = matrix(se_X2 %>% sqrt, ncol = sqrt(nrow(dat2))), col = heat.colors(130),
      main = "Standard Deviation Surface", xlab = "x1", ylab = "x2")
points(x = center[1], y = center[2], pch = 20)
points(x = center[1], y = center[2], col = 'white')
se_range = range(se_X2) %>% sqrt
mtext(paste("min: ", round(se_range[1], 4),
            ", max: ", round(se_range[2], 4), sep = ""), side = 4)
par(mfcol = c(1,1))

rmse = sqrt(mean((y_pred - pred_X)^2))
rmse_truth = sqrt(mean((dat2$y - pred_X)^2))
check = aGPsep(X = X, Z = y_obs, XX = dat2[,1:2], omp.threads = 8)
rmse_laGP = sqrt(mean((check$mean-dat2$y)^2))
rmse_obs = sqrt(mean((y_pred - dat2$y)^2))
sum(dat_obs$y > pred_X - 1.96*sqrt(se_pred) & dat_obs$y < pred_X + 1.96*sqrt(se_pred))/length(dat$y)
sum(dat_obs$y > pred_X - 1.96*sqrt(se_pred2) & dat_obs$y < pred_X + 1.96*sqrt(se_pred2))/length(dat$y)


ex_2d = list(pred = c(score(Y = y_pred, mu = pred_X, s2 = se_pred),
                      score(Y = y_pred, mu = pred_X, s2 = se_pred2),
                      score(Y = y_pred, mu = check$mean, s2 = check$var)),
             mean = c(score(Y = dat2$y, mu = pred_X, s2 = se_X),
                      score(Y = dat2$y, mu = pred_X, s2 = se_X2)))
print(ex_1d)
print(ex_2d)
