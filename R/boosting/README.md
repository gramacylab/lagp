## This directory contains code supporing boosting of laGP predictors

### Reading list

- Cressie paper on adaptive dispersion clustering: http://www.sciencedirect.com/science/article/pii/S0167947317301731
- Apley paper on random partitioning of Kriging predictors: https://arxiv.org/pdf/1701.06655.pdf
- Paper on aggregating sub-sampled Kriging predictors: https://arxiv.org/pdf/1607.05432.pdf
- Rushdi, A., Swiler, L., Phipps, E., D'Elia, M., & Ebeida, M. VPS: Voronoi Piecewise Surrogate Models for High-Dimensional Data Fitting. International Journal for Uncertainty Quantification
