if(getwd()=="/home/adam") setwd("/home/adam/lagp/R/boosting")
source("functions.R")

index = menu(choices = c("Gramacy and Lee", "Herbie's Molar", "Michalewicz"), title = "Which function")

if(index==3){
  d = readline("Number of Dimensions = ") %>% as.numeric
}else{
  xlim = readline("X1 limits (separate by comma) = ") %>% sapply(function(x) strsplit(x, split = ",")) %>% unlist %>% as.numeric
  ylim = readline("X2 limits (separate by comma) = ") %>% sapply(function(x) strsplit(x, split = ",")) %>% unlist %>% as.numeric
}

okay = F
while(!okay){
  n = readline("n = ") %>% as.numeric
  if(index == 3){
    size = n^d
  }else{
    size = n^2
  }
  check = menu(choices = c("Yes", "No"), title = paste("Final size :", size, "\nIs this okay?"))
  if(check == 1) okay = T
}

if(index == 1){
  rm(list = ls()[grep("g_", ls())])
  g_dat = gramacy(n = n, domain = rbind(xlim, ylim))
  g_dat2 = gramacy(n = n+1, domain = rbind(xlim, ylim))
  g_dat3 = gramacy(n = n-1, domain = rbind(xlim, ylim))
}
if(index == 2){
  rm(list = ls()[grep("m_", ls())])
  h_dat = f2d(n = n, domain = rbind(xlim, ylim))
  h_dat2 = f2d(n = n+1, domain = rbind(xlim, ylim))
  h_dat3 = f2d(n = n-1, domain = rbind(xlim, ylim))
}
if(index == 3){
  rm(list = ls()[grep("m_", ls())])
  m = readline("m = ") %>% as.numeric
  m_dat = michal(n = n, m = m, d = d)
  m_dat2 = michal(n = n+1, m = m, d = d)
  m_dat3 = michal(n = n-1, m = m, d = d)
}