if(getwd()=="/home/adam") setwd("/home/adam/lagp/R/boosting")
source("functions.R")
if(sum(grepl("_dat", ls()))==0){
  print("No Data Generated", quote = F)
  source("gen.R")
}

par(mfcol = c(1,1))

if(sum(grepl("_dat", ls()) & !grepl("3", ls()) & !grepl("2", ls())) == 1){
  tests = ls()[grepl("_dat", ls()) & !grepl("3", ls()) & !grepl("2", ls())]
}else{
  select = menu(choices = c("All", ls()[grepl("_dat", ls()) & !grepl("3", ls()) & !grepl("2", ls())]), title = "Data Selection :")
  if(select == 1){
    tests = ls()[grepl("_dat", ls()) & !grepl("3", ls()) & !grepl("2", ls())]
  }else{
    tests = ls()[grepl("_dat", ls()) & !grepl("3", ls()) & !grepl("2", ls())][select-1]
  }
}

n.test = readline("Number of steps (5 points each) = ") %>% as.numeric

export = menu(choices = c("Yes", "No"), title = "Export Plots?") == 1

if(export){
  pdf(file = "tempincrease.pdf", onefile = T)
}

for(k in tests){
  temp = .GlobalEnv[[k]]
  X = temp[,-ncol(temp)]
  y = temp$y
  temp2 = .GlobalEnv[[paste(k, 2, sep = "")]]
  XX = temp2[,-ncol(temp2)]
  yy = temp2$y 
  ind = strsplit(k, "_")[[1]][1]
  if(paste(ind, "dmax", sep = "") %in% ls()){
    use = menu(choices = c("Yes", "No"), title = "Use existing dmax?") == 1
    if(use){
      dmax = .GlobalEnv[[paste(ind, "dmax", sep = "_")]]
    }else{
      dmax = NULL
    }
  }else{dmax = NULL}
  gmle = ind == "g"
  .GlobalEnv[[paste(ind, "mod", sep = "_")]] = .GlobalEnv[[paste(ind, "preds", sep = "_")]] = list()
  C = maximin(range = cbind(apply(X, 2, min), apply(X, 2, max)), t = 10000, n = 5)
  .GlobalEnv[[paste(ind, "mod", sep = "_")]][["model1"]] = temp.mod = kalm(X = X, y = y, C = C, f = fit, gmle = gmle, dmax = dmax)
  .GlobalEnv[[paste(ind, "preds", sep = "_")]][[paste("preds", "1", sep = "")]] = temp.preds = predict(temp.mod, XX = XX)
  cols2 = heat.colors(128)
  dt1 = data.table(cbind(XX, preds = temp.preds$mean))[,.(surface = mean(preds)), by = .(x1,x2)]
  image(x = XX$x1 %>% unique, y = XX$x2 %>% unique, 
        z = matrix(dt1$surface, ncol = length(unique(XX$x2))), col = cols2,
        main = paste(ind, " Model\n", nrow(XX), " points, ",
                     nrow(temp.mod$centers), " models", sep = ""), xlab = "x1", ylab = "x2")
  points(x = temp.mod$centers[,1], y = temp.mod$centers[,2], pch = 20)
  for(i in 2:n.test){
    C = maximin(range = cbind(apply(X, 2, min), apply(X, 2, max)), t = 10000, n = 5*i, data = C)
    .GlobalEnv[[paste(ind, "mod", sep = "_")]][[paste("model", i, sep = "")]] = temp.mod =
      update(temp.mod, Xnew = X, ynew = y, Cnew = C[(5*(i-1)+1):(5*i),])
    .GlobalEnv[[paste(ind, "preds", sep = "_")]][[paste("preds", i, sep = "")]] = temp.preds = predict(temp.mod, XX = XX)
    dt1 = data.table(cbind(XX, preds = temp.preds$mean))[,.(surface = mean(preds)), by = .(x1,x2)]
    image(x = XX$x1 %>% unique, y = XX$x2 %>% unique, 
          z = matrix(dt1$surface, ncol = length(unique(XX$x2))), col = cols2,
          main = paste(ind, " Model\n", nrow(XX), " points, ",
                       nrow(temp.mod$centers), " models", sep = ""), xlab = "x1", ylab = "x2")
    points(x = temp.mod$centers[,1], y = temp.mod$centers[,2], pch = 20)
  }
  sdm = menu(choices = c("Yes", "No"), title = "Save dmax?") == 1
  if(sdm){
    .GlobalEnv[[paste(ind, "dmax", sep = "")]] = .GlobalEnv[[paste(ind, "mod", sep = "_")]][[1]]$dmax
  }
  .GlobalEnv[[paste(ind, "sequence", sep = "_")]] = lapply(.GlobalEnv[[paste(ind, "preds", sep = "_")]],
                                                           function(x) score(Y = yy, mu = x$mean, s2 = x$s2)) %>% unlist
}

par(mfcol = c(1,sum(grepl("sequence", ls()))))
for(k in ls()[grepl("sequence", ls())]){
  ind = strsplit(k, "_")[[1]][1]
  plot(x = 5*1:length(.GlobalEnv[[k]]), y = .GlobalEnv[[k]], type = 'l', ylab = "centers", xlab = "score", main = paste(ind, "Model"))
}
par(mfcol = c(1,1))

if(export){
  dev.off()
}

rm(list = c("temp.mod", "temp.preds"))
