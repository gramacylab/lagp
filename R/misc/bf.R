data <- read.csv("../../data/bf.csv")[,-1]
y <- data[,9]
X <- data[,-9]

library(laGP)
gpisep <- newGPsep(X, y, d=0.1, g=1e-6, dK=TRUE)
mle <- mleGPsep(gpisep)

N <- 10000
G <- 30  
m <- q1 <- q2 <- matrix(NA, ncol=ncol(X), nrow=G)
grid <- seq(0, 1, length=G)
XX <- matrix(NA, ncol=ncol(X), nrow=N)

library(lhs)
for(j in 1:ncol(X)) {
  for(i in 1:G) {
    XX[,j] <- grid[i]
    XX[,-j] <- randomLHS(N, ncol(X)-1)
    p <- predGPsep(gpisep, XX, lite=TRUE, nonug=TRUE)
    m[i,j] <- mean(p$mean)
    q1[i,j] <- mean(qnorm(0.05, p$mean, sqrt(p$s2)))
    q2[i,j] <- mean(qnorm(0.95, p$mean, sqrt(p$s2)))
  }
}

plot(0, xlab="grid", ylab="main effect", main="Bayes Factor Main Effects", xlim=c(0,1), ylim=range(c(q1,q2)), type="n")
for(j in 1:ncol(X)) {
  lines(grid, m[,j], col=j, lwd=2)
  lines(grid, q1[,j], col=j, lty=2)
  lines(grid, q2[,j], col=j, lty=2) 
}
legend("topright", names(X), fill=1:ncol(X))

M <- randomLHS(N, ncol(X))
pM <- predGPsep(gpisep, M, lite=TRUE, nonug=TRUE)
Ey <- mean(pM$mean)
Vary <- (t(pM$mean) %*% pM$mean)/N - Ey^2

Mprime <- randomLHS(N, ncol(X))
S <- EE2j <- rep(NA, ncol(X))
for(j in 1:ncol(X)) {
  Mjprime <- Mprime
  Mjprime[,j] <- M[,j]
  pMprime <- predGPsep(gpisep, Mjprime, lite=TRUE, nonug=TRUE)
  EE2j[j] <- (t(pM$mean) %*% pMprime$mean)/(N-1)
  S[j] <- (EE2j[j] - Ey^2)/Vary
}

S[S < 0] <- 0
S

T <- EE2mj <- rep(NA, ncol(X))
for(j in 1:ncol(X)) {
  Mj <- M
  Mj[,j] <- Mprime[,j]
  pMj <- predGPsep(gpisep, Mj, lite=TRUE, nonug=TRUE)
  EE2mj[j] <- (t(pM$mean) %*% pMj$mean)/(N-1)
  T[j] <- 1 - (EE2mj[j] - Ey^2)/Vary
}

T[T < 0] <- 0
T

I <- T-S
I[I < 0] <- 0
I

m <- q1 <- q2 <- matrix(NA, ncol=ncol(X), nrow=G)
grid <- seq(0, 1, length=G)
XX <- matrix(NA, ncol=ncol(X), nrow=G)

## similar to main effect loop above, but with fixed midway values
for(j in 1:ncol(X)) {
  XX[,j] <- grid
  XX[,-j] <- 0.5
  p <- predGPsep(gpisep, XX, lite=TRUE, nonug=TRUE)
  m[,j] <- p$mean
  q1[,j] <- qnorm(0.05, p$mean, sqrt(p$s2))
  q2[,j] <- qnorm(0.95, p$mean, sqrt(p$s2))
}

## Chris' conversion back to natural units
XXnat <- as.data.frame(XX)
colnames(XXnat) <- c("g", "mu01", "mu02", "lam011", "lam022", "lam0OD", "sigsq0", "nu0")
XXnat$g=      99*grid + 1
XXnat$mu01=   50*grid + 20
XXnat$mu02=   3*grid + 0.5
XXnat$lam011= 20*grid + 15
XXnat$lam022= 10*grid + 5
XXnat$lam0OD= 1.25*grid + .75
XXnat$sigsq0= 50*grid + 60
XXnat$nu0 =   1.9*grid + .1

par(mfrow=c(3,3))
for(j in 1:ncol(XXnat)) {
  ylim <- c(min(m[,j])-0.5, max(m[,j])+0.5)
  if(min(q1[,j]) < ylim[1]) ylim[1] <- min(q1[,j])
  if(max(q2[,j]) > ylim[2]) ylim[2] <- max(q2[,j])
  plot(XXnat[,j], m[,j], type="l", xlab=names(XXnat)[j], ylab="log Bayes factor", ylim=ylim)
  lines(XXnat[,j], q1[,j], col=2, lty=2)
  lines(XXnat[,j], q2[,j], col=2, lty=2)
}

deleteGPsep(gpi)