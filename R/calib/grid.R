source("calib.R")


## eta:
##
## computer model test func˘on (aka "Toy example") used in Goh et al, 2013
## (to appear in Technometrics)is a more elaborate version of the test 
## function used in Bastos and O'Hagan, 2009, Technometrics
##
## x = the design variable inputs .... here is n x 2
## t = calibration variable inputs .... here is n x 2
## both (x,t) lives on the 4-d unit cube
    
eta <- function(x,t) 
  {
    x <- as.matrix(x)
    t <- as.matrix(t)
    out <- (1-exp(-1/(2*x[,2]))) 
    out <- out * (1000*t[,1]*x[,1]^3+1900*x[,1]^2+2092*x[,1]+60) 
    out <- out / (100*t[,2]*x[,1]^3+500*x[,1]^2+4*x[,1]+20)  
    return(out)
  }

  
## delta:  
##    
## discrepancy function used in the "Toy example" of Goh et al, 2013 (to
##  appear in Technometrics) x = the design variable inputs; here is n x 2
## x belongs to the unit square... or [0,1] x [0,1]  

delta <- function(x) 
  {
    x<-as.matrix(x)   
    ## out<- (10*x[,1]^2+4*x[,2]^2) / (50*x[,1]*x[,2]+10)
    ## larger discrepency; changed by Bobby
    out<- 2*(10*x[,1]^2+4*x[,2]^2) / (50*x[,1]*x[,2]+10)
    return(out)
  }


## tgp for LHS sampling
library(tgp)
rect <- matrix(rep(0:1, 4), ncol=2, byrow=2)

## draw LHS for the physical model
ny <- 50; nny <- 1000  
X <- lhs(ny, rect[1:2,])
XX <- lhs(nny, rect[1:2,],)
theta <- c(0.2, 0.1)
Ztheta <- eta(X, matrix(theta, nrow=1))	
ZZtheta <- eta(XX, matrix(theta, nrow=1))	
sd <- 0.1 ## Originally 0.5 -- smaller sd for noise; changed by Bobby
## Y <- rep(Ztheta, 2) + rnorm(2*length(Ztheta), sd=sd) ## UNBIASED REPLICATED 
## Y <- Ztheta + rnorm(length(Ztheta), sd=sd) ## UNBIASED 
## Y <- Ztheta + delta(X) + rnorm(length(Ztheta), sd=sd) ## BIASED 
Y <- rep(Ztheta,2) + rep(delta(X),2) + rnorm(2*length(Ztheta), sd=sd) ## BIASED REPLICATED 

## draw a LHS for the computer model
nz <- 10000
XT <- lhs(nz, rect)

## augment with physical model design points 
## with various theta settings
XT2 <- matrix(NA, nrow=10*ny, ncol=4)
for(i in 1:10) {
	I <- ((i-1)*ny+1):(ny*i)
	XT2[I,1:2] <- X
}
XT2[,3:4] <- lhs(10*ny, rect[3:4,])
XT <- rbind(XT, XT2)

## evaluate the computer model
Z <- eta(XT[,1:2], XT[,3:4])

## priors and approxGP settings
methods <- FALSE ## c("nn", "alc", "alc") ## FALSE for no eta-hat (use eta directly)
d <- darg(NULL, XT, samp.size=nrow(XT))
da <- d; d$ab <- c(0,0)
g <- garg(list(mle=TRUE), Y) ## could also use Ym

## try a grid of tuning parameter values
cat("eval on space-filling grid\n")
t <- seq(0,1,length=41)
tgrid <- expand.grid(t, t)
ll.mlegp <- ll <- rep(NA, nrow(tgrid))
for(i in 1:nrow(tgrid)) {
	ll[i] <- fcalib(tgrid[i,], XT, Z, X, Y, da, d, g, methods, TRUE)
	ll.mlegp[i] <- fcalib(tgrid[i,], XT, Z, X, Y, da, d, g, methods, "mlegp")
}

## make a plot comparing mlegp to Bobby's GP
par(mfrow=c(1,2))
image(t, t, matrix(-ll, ncol=length(t)), col=heat.colors(128), 
	main="loglik: Isotropic discrep", xlab="t1", ylab="t2")
image(t, t, matrix(-ll.mlegp, ncol=length(t)), col=heat.colors(128),
	main="loglik: Separable (mlegp) discrep", xlab="t1", ylab="t2")