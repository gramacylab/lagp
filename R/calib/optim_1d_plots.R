
  theta <- c(0.2, 0.1)

  ## input grid
  G <- 100
  tinit <- matrix(NA, ncol=2, nrow=G*2)
  tinit[1:G,1] <- seq(0.01, 0.99, length=G)
  tinit[1:G,2] <- theta[2]
  tinit[(G+1):(2*G),2] <- seq(0.01, 0.99, length=G)
  tinit[(G+1):(2*G),1] <- theta[1]


## pdf("unbiased_calib_1d.pdf", width=5, height=5)
par(mfrow=c(1,2), mar=c(5,0.5,0.5,0.25))

lleh <- - read.table("calib_1d_unbiased_etahat_noreps/ll.txt", header=FALSE)
lleh10 <- - read.table("calib_1d_unbiased_etahat_10reps/ll.txt", header=FALSE)
lle10 <- - read.table("calib_1d_unbiased_eta_10reps/ll.txt", header=FALSE)

L1eh <- exp(lleh[,1:G] - apply(lleh[,1:G], 1, max))
L1ehm <- apply(L1eh, 2, mean)

plot(tinit[1:G,1], L1ehm/sum(L1ehm), ## type="l", 
  ylim=c(0,0.1), ## lty=1, lwd=2,
  bty="n", yaxt="n", ylab ="", ## ylab="likelihood",
   xlab="u1 | u2 = 0.1")

L1e10 <- exp(lle10[,1:G] - apply(lle10[,1:G], 1, max))
L1e10m <- apply(L1e10, 2, mean)

## lines(tinit[1:G,1], L1e10m/sum(L1e10m), col=3, lty=3, lwd=2)
points(tinit[1:G,1], L1e10m/sum(L1e10m), col=3, pch=13)

L1eh10 <- exp(lleh10[,1:G] - apply(lleh10[,1:G], 1, max))
L1eh10m <- apply(L1eh10, 2, mean)

## lines(tinit[1:G,1], L1eh10m/sum(L1eh10m), col=2, lty=2, lwd=2)
points(tinit[1:G,1], L1eh10m/sum(L1eh10m), col=2, pch=16)

L2eh <- exp(lleh[,(G+1):(2*G)] - apply(lleh[,(G+1):(2*G)], 1, max))
L2ehm <- apply(L2eh, 2, mean)

abline(v=0.2, col=4, lty=2)
x <- seq(0.01, 0.99, length=100)
db <- dbeta(x, 2, 2)
lines(x, db/sum(db), col="gray", lwd=2)

plot(tinit[(G+1):(2*G),2], L2ehm/sum(L2ehm), ## type="l", 
  ylim=c(0,0.1), ## lty=1, lwd=2,
  bty="n", yaxt="n",  ylab="", xlab="u2 | u1 = 0.2")

L2e10 <- exp(lle10[,(G+1):(2*G)] - apply(lle10[,(G+1):(2*G)], 1, max))
L2e10m <- apply(L2e10, 2, mean)

## lines(tinit[(G+1):(2*G),2], L2e10m/sum(L2e10m), col=3, lty=3, lwd=2)
points(tinit[(G+1):(2*G),2], L2e10m/sum(L2e10m), col=3, pch=13)

L2eh10 <- exp(lleh10[,(G+1):(2*G)] - apply(lleh10[,(G+1):(2*G)], 1, max))
L2eh10m <- apply(L2eh10, 2, mean)

## lines(tinit[(G+1):(2*G),2], L2eh10m/sum(L2eh10m), col=2, lty=2, lwd=2)
points(tinit[(G+1):(2*G),2], L2eh10m/sum(L2eh10m), col=2, pch=16)

## legend("topright", c("uhat-Mhat", "uhat-Mhat-10", "uhat-M-10"), bty="n",
  ## lty=1:3, col=1:3, lwd=2)
legend("top", c("prior", "truth"), col=c("gray", "blue"), 
  lty=c(1,2), lwd=2, bty="n")

abline(v=0.1, col=4, lty=2)
lines(x, db/sum(db), col="gray", lwd=2)

## dev.off()

## pdf("biased_calib_1d.pdf", width=5, height=5)
par(mfrow=c(1,2), mar=c(5,0.5,0.5,0.25))

lleh <- - read.table("calib_1d_biased_etahat_noreps/ll.txt", header=FALSE)
lleh10 <- - read.table("calib_1d_biased_etahat_10reps/ll.txt", header=FALSE)
lle10 <- - read.table("calib_1d_biased_eta_10reps/ll.txt", header=FALSE)

L1eh <- exp(lleh[,1:G] - apply(lleh[,1:G], 1, max))
L1ehm <- apply(L1eh, 2, mean)

plot(tinit[1:G,1], L1ehm/sum(L1ehm), ## type="l", 
  ylim=c(0,0.028), ## lty=1, lwd=2,
  bty="n", yaxt="n", ylab ="", ## ylab="likelihood",
   xlab="u1 | u2 = 0.1")

L1e10 <- exp(lle10[,1:G] - apply(lle10[,1:G], 1, max))
L1e10m <- apply(L1e10, 2, mean)

## lines(tinit[1:G,1], L1e10m/sum(L1e10m), col=3, lty=3, lwd=2)
points(tinit[1:G,1], L1e10m/sum(L1e10m), col=3, pch=13)

L1eh10 <- exp(lleh10[,1:G] - apply(lleh10[,1:G], 1, max))
L1eh10m <- apply(L1eh10, 2, mean)

## lines(tinit[1:G,1], L1eh10m/sum(L1eh10m), col=2, lty=2, lwd=2)
points(tinit[1:G,1], L1eh10m/sum(L1eh10m), col=2, pch=16)

L2eh <- exp(lleh[,(G+1):(2*G)] - apply(lleh[,(G+1):(2*G)], 1, max))
L2ehm <- apply(L2eh, 2, mean)

abline(v=0.2, col=4, lty=2)
x <- seq(0.01, 0.99, length=100)
db <- dbeta(x, 2, 2)
lines(x, db/sum(db), col="gray", lwd=2)

plot(tinit[(G+1):(2*G),2], L2ehm/sum(L2ehm), ## type="l", 
  ylim=c(0,0.028), ## lty=1, lwd=2,
  bty="n", yaxt="n",  ylab="", xlab="u2 | u1 = 0.2")

L2e10 <- exp(lle10[,(G+1):(2*G)] - apply(lle10[,(G+1):(2*G)], 1, max))
L2e10m <- apply(L2e10, 2, mean)

## lines(tinit[(G+1):(2*G),2], L2e10m/sum(L2e10m), col=3, lty=3, lwd=2)
points(tinit[(G+1):(2*G),2], L2e10m/sum(L2e10m), col=3, pch=13)

L2eh10 <- exp(lleh10[,(G+1):(2*G)] - apply(lleh10[,(G+1):(2*G)], 1, max))
L2eh10m <- apply(L2eh10, 2, mean)

## lines(tinit[(G+1):(2*G),2], L2eh10m/sum(L2eh10m), col=2, lty=2, lwd=2)
points(tinit[(G+1):(2*G),2], L2eh10m/sum(L2eh10m), col=2, pch=16)

## legend("topright", c("uhat-Mhat", "uhat-Mhat-10", "uhat-M-10"), bty="n",
  ## lty=1:3, col=1:3, lwd=2)
legend("top", c("prior", "truth"), col=c("gray", "blue"), 
  lty=c(1,2), lwd=2, bty="n")

abline(v=0.1, col=4, lty=2)
lines(x, db/sum(db), col="gray", lwd=2)

## dev.off()