## This file is to combine field, NAM (plus its discrepancy), and SREF (plus its discrepancy) 
## using Inverse-variance weighting (IVW) to perform out-of-sample prediction in the context of LOO-CV.
## This file compiles partial results of **Table 1**.
## To obtain the corresponding result, change the arguments accordingly.

library(laGP)
library(maps)
library(plgp, quietly=TRUE, warn.conflicts=FALSE)

source("function.R")

## the time-aggregated output
sol_ta <- read.csv("../../data/solance/sol_ta.csv")
## the 1535 known spatial locations
lola_kn <- read.csv("../../data/solance/lola.csv")
## convert data.frame to matrix
lola_kn <- as.matrix(lola_kn)

yf <- sol_ta[,1] ## field
yN <- sol_ta[,2] ## NAM
yS <- sol_ta[,3] ## SREF

#### treat the computer model outputs as unknown, and predict it using different GP emulators ####
yg.N <- yh.N <- yg.v.N <- yh.v.N <- rep(NA, length(yf))
yg.S <- yh.S <- yg.v.S <- yh.v.S <- rep(NA, length(yf)) 
te <- sqrt(.Machine$double.eps)
for(i in 1:nrow(lola_kn)){
  
    ## testing and training sets: input space
    Xtest <- matrix(lola_kn[i,], nrow=1)
    Xtrain <- lola_kn[-i,]
  
    ## training responses (NAM)
    ytrain.N <- yN[-i]
    ## training responses (SREF)
    ytrain.S <- yS[-i]
    
    ## scale the input space to unit cube
    maxX <- apply(Xtrain, 2, max)
    minX <- apply(Xtrain, 2, min)
    for(j in 1:ncol(Xtrain)){
        Xtrain[,j] <- Xtrain[,j] - minX[j]
        Xtrain[,j] <- Xtrain[,j]/(maxX[j]-minX[j])
    
        Xtest[,j] <- Xtest[,j] - minX[j]
        Xtest[,j] <- Xtest[,j]/(maxX[j]-minX[j])
    }
    
    ## generate lengthscale prior
    da <- darg(list(mle=TRUE), Xtrain)
  
    ############################# NAM ##################################
    nam <- gp_gh_dt(X=Xtrain, y=ytrain.N, XX=Xtest, da=da, ga=1e-6, eps=te, nth=1, ve=0)
    yg.N[i] <- nam$gm
    yg.v.N[i] <- nam$gv
    yh.N[i] <- nam$hm
    yh.v.N[i] <- nam$hv
    
    ############################# SREF ##################################
    sref <- gp_gh_dt(X=Xtrain, y=ytrain.S, XX=Xtest, da=da, ga=1e-6, eps=te, nth=1, ve=0)
    yg.S[i] <- sref$gm
    yg.v.S[i] <- sref$gv
    yh.S[i] <- sref$hm
    yh.v.S[i] <- sref$hv
    
    save(yg.N, yh.N, yg.v.N, yh.v.N, 
         yg.S, yh.S, yg.v.S, yh.v.S,
         file="ypred_N-S.RData")
    print(i)
}

load("ypred_N-S.RData")

## RMSE
pm.NAM <- as.matrix(data.frame(g=yg.N, h=yh.N))
rmse.NAM <- apply(pm.NAM, 2, rmse.cal, true=yN)
## rmse.NAM
##      g        h         
## 10.172047  9.713202
pm.SREF <- as.matrix(data.frame(g=yg.S, h=yh.S))
rmse.SREF <- apply(pm.SREF, 2, rmse.cal, true=yS)
## rmse.SREF
##     g         h
## 10.50685  10.02340

## RMSE between computer model (deterministic or fitted) and field data: to demonstrate the benefit of emulating discrepancy

##### emulated computer model
rmse.N_f <- apply(pm.NAM, 2, rmse.cal, true=yf) ## (line 5 of Table 1: NAM.hat w/o b)
## rmse.N_f
##     g        h     
## 33.62302 33.48272
rmse.S_f <- apply(pm.SREF, 2, rmse.cal, true=yf) ## (line 4 of Table 1: SREF.hat w/o b)
## rmse.S_f
##     g        h   
## 45.62884 45.43205

##### 95% predictive coverage (line 4 of Table 1: SREF.hat w/o b)
### global
cov95_sref_g.per <- covper(true=yf, mean=yg.S, var=yg.v.S, perc=95)
# cov95_sref_g.per
# 0.1856678
### local
cov95_sref_h.per <- covper(true=yf, mean=yh.S, var=yh.v.S, perc=95)
# cov95_sref_h.per
# 0.1752443

##### 95% predictive coverage (line 5 of Table 1: NAM.hat w/o b)
### global
cov95_nam_g.per <- covper(true=yf, mean=yg.N, var=yg.v.N, perc=95)
# cov95_nam_g.per
# 0.4436482
### local
cov95_nam_h.per <- covper(true=yf, mean=yh.N, var=yh.v.N, perc=95)
# cov95_nam_h.per
# 0.3511401

##### deterministic computer model
rmse.N_f_dt <- apply(as.data.frame(yN), 2, rmse.cal, true=yf)
## rmse.N_f_dt
## 32.79534
rmse.S_f_dt <- apply(as.data.frame(yS), 2, rmse.cal, true=yf)
## rmse.S_f_dt
## 44.96667

########################################### IVW using deterministic simulation output ###########################################
yft.g <- yft.h <- rep(NA, nrow(lola_kn))
vft.g <- vft.h <- rep(NA, nrow(lola_kn))
eps <- sqrt(.Machine$double.eps)
for(i in 1:nrow(lola_kn)){
    
    #################### testing and training partition #############
    ## input space
    Xtest <- matrix(lola_kn[i,], nrow=1)
    Xtrain <- lola_kn[-i,]
    ## responses: field data
    ytrain.f <- yf[-i]
    ## responses: NAM
    ytest.N <- yN[i]; ytrain.N <- yN[-i]
    ## responses: SREF
    ytest.S <- yS[i]; ytrain.S <- yS[-i]
    ## field - NAM
    ytrain.dis.n <- ytrain.f - ytrain.N
    ## field - SREF
    ytrain.dis.s <- ytrain.f - ytrain.S
    
    ############ scale the input space to the unit cube ############
    maxX <- apply(Xtrain, 2, max)
    minX <- apply(Xtrain, 2, min)
    for(j in 1:ncol(Xtrain)){
        Xtrain[,j] <- Xtrain[,j] - minX[j]
        Xtrain[,j] <- Xtrain[,j]/(maxX[j]-minX[j])
        
        Xtest[,j] <- Xtest[,j] - minX[j]
        Xtest[,j] <- Xtest[,j]/(maxX[j]-minX[j])
    }
    
    da <- darg(list(mle=TRUE), Xtrain, samp.size=nrow(Xtrain))            
    ga.field <- garg(list(mle=TRUE), ytrain.f)     
    ga.dis.n <- garg(list(mle=TRUE), ytrain.dis.n) 
    ga.dis.s <- garg(list(mle=TRUE), ytrain.dis.s)
    
    ######################## fit the field data ####################
    field <- gp_gh_un(X=Xtrain, y=ytrain.f, XX=Xtest, da=da, ga=ga.field, eps=eps, nth=1, ve=0)
    
    ## calculate the predictive mean for field data at each testing location
    yf.g <- field$gm
    yf.h <- field$hm
    
    yf.v.g <- field$gv
    yf.v.h <- field$hv
    
    ######################## fit the discrepancy between field and NAM ###################
    dis.n <- gp_gh_un(X=Xtrain, y=ytrain.dis.n, XX=Xtest, da=da, ga=ga.dis.n, eps=eps, nth=1, ve=0)
    
    yfn.g <- dis.n$gm + ytest.N
    yfn.h <- dis.n$hm + ytest.N
    
    yfn.v.g <- dis.n$gv
    yfn.v.h <- dis.n$hv
    
    ######################## fit the discrepancy between field and SREF ###################
    dis.s <- gp_gh_un(X=Xtrain, y=ytrain.dis.s, XX=Xtest, da=da, ga=ga.dis.s, eps=eps, nth=1, ve=0)
    
    yfs.g <- dis.s$gm + ytest.S
    yfs.h <- dis.s$hm + ytest.S
    
    yfs.v.g <- dis.s$gv
    yfs.v.h <- dis.s$hv
    
    ################# combined predictive means and predictive variances #################
    ft.g <- inv.pred.sing(mu=c(yf.g, yfn.g, yfs.g), sg2=c(yf.v.g, yfn.v.g, yfs.v.g))
    ft.h <- inv.pred.sing(mu=c(yf.h, yfn.h, yfs.h), sg2=c(yf.v.h, yfn.v.h, yfs.v.h))
    
    yft.g[i] <- ft.g$m
    yft.h[i] <- ft.h$m
      
    vft.g[i] <- ft.g$v
    vft.h[i] <- ft.h$v
    
    save(yft.g, yft.h, vft.g, vft.h, file="inv-pred.RData")
    print(i)
}

load("inv-pred.RData")

##### RMSE (line 11 of Table 1)
pm.inv <- as.matrix(data.frame(g=yft.g, h=yft.h))
rmse.inv <- apply(pm.inv, 2, rmse.cal, true=yf)
## rmse.inv
##     g       h         
## 23.63420 23.40557 

pv.inv <- as.matrix(data.frame(g=vft.g, h=vft.h))
score.inv <- rep(NA, ncol(pm.inv))
for(i in 1:length(score.inv)){
    score.inv[i] <- score.cal(m=pm.inv[,i], v=pv.inv[,i], true=yf)
}
## score.inv
##     g       h
## 8.139961 8.644929

## 95% predictive coverage (line 11 of Table 1: IVW)
# global
cov95_ivw_g.per <- covper(true=yf, mean=yft.g, var=vft.g, perc=95)
# cov95_ivw_g.per
# 0.8325733

# local
cov95_ivw_l.per <- covper(true=yf, mean=yft.h, var=vft.h, perc=95)
# cov95_ivw_l.per
# 0.8104235

######## IVW.fit experiment using emulated discrepancy from 'unknown' computer models ######## 
load("ypred_N-S.RData") ## LOO-CV results for computer models
## yg.N, yh.N
## yg.S, yh.S

## yg.v.N, yh.v.N
## yg.v.S, yh.v.S

## discrepancy terms from emulated computer models
ydis_N.g <- yf - yg.N
ydis_N.h <- yf - yh.N
ydis_S.g <- yf - yg.S
ydis_S.h <- yf - yh.S

yft.g <- yft.h <- rep(NA, nrow(lola_kn))
vft.g <- vft.h <- rep(NA, nrow(lola_kn))
te <- sqrt(.Machine$double.eps)
for(i in 1:nrow(lola_kn)){
  
  Xtest <- matrix(lola_kn[i,], nrow=1)
  Xtrain <- lola_kn[-i,]
  
  ytrain.f <- yf[-i]
  ytrain.N <- yN[-i]
  ytrain.S <- yS[-i]
  ytrain.dis.n.g <- ydis_N.g[-i]
  ytrain.dis.n.h <- ydis_N.h[-i]
  ytrain.dis.s.g <- ydis_S.g[-i]
  ytrain.dis.s.h <- ydis_S.h[-i]
  
  maxX <- apply(Xtrain, 2, max)
  minX <- apply(Xtrain, 2, min)
  for(j in 1:ncol(Xtrain)){
    Xtrain[,j] <- Xtrain[,j] - minX[j]
    Xtrain[,j] <- Xtrain[,j]/(maxX[j]-minX[j])
    Xtest[,j] <- Xtest[,j] - minX[j]
    Xtest[,j] <- Xtest[,j]/(maxX[j]-minX[j])
  }
  
  da.all <- darg(list(mle=TRUE), Xtrain, samp.size=nrow(Xtrain))             
  ga.f <- garg(list(mle=TRUE), ytrain.f)     
  ga.dis.n.g <- garg(list(mle=TRUE), ytrain.dis.n.g)
  ga.dis.n.h <- garg(list(mle=TRUE), ytrain.dis.n.h)
  ga.dis.s.g <- garg(list(mle=TRUE), ytrain.dis.s.g)
  ga.dis.s.h <- garg(list(mle=TRUE), ytrain.dis.s.h)
  
  ## NAM discrepancy: global
  nam.dis.g <- gp_gh_un(X=Xtrain, y=ytrain.dis.n.g, XX=Xtest, da=da.all, ga=ga.dis.n.g, eps=te, nth=1, ve=0) 
  ## NAM discrepancy: hybrid
  nam.dis.h <- gp_gh_un(X=Xtrain, y=ytrain.dis.n.h, XX=Xtest, da=da.all, ga=ga.dis.n.h, eps=te, nth=1, ve=0)
  
  ## SREF discrepancy: global
  sref.dis.g <- gp_gh_un(X=Xtrain, y=ytrain.dis.s.g, XX=Xtest, da=da.all, ga=ga.dis.s.g, eps=te, nth=1, ve=0)
  ## SREF discrepancy: hybrid
  sref.dis.h <- gp_gh_un(X=Xtrain, y=ytrain.dis.s.h, XX=Xtest, da=da.all, ga=ga.dis.s.h, eps=te, nth=1, ve=0)
  
  ## field
  f <- gp_gh_un(X=Xtrain, y=ytrain.f, XX=Xtest, da=da.all, ga=ga.f, eps=te, nth=1, ve=0)
  
  ## combine emulated computer models and emulated discrepancy terms
  ### predictive mean
  yfn.g <- yg.N[i] + nam.dis.g$gm
  yfn.h <- yh.N[i] + nam.dis.h$hm
  
  yfs.g <- yg.S[i] + sref.dis.g$gm
  yfs.h <- yh.S[i] + sref.dis.h$hm
  
  ## predictive variance
  yfn.v.g <- yg.v.N[i] + nam.dis.g$gv
  yfn.v.h <- yh.v.N[i] + nam.dis.h$hv
  
  yfs.v.g <- yg.v.S[i] + sref.dis.g$gv
  yfs.v.h <- yh.v.S[i] + sref.dis.h$hv
  
  ################# combined predictive means and predictive variances #################
  ft.g <- inv.pred.sing(mus=c(f$gm, yfn.g, yfs.g), sg2s=c(f$gv, yfn.v.g, yfs.v.g))
  ft.h <- inv.pred.sing(mus=c(f$hm, yfn.h, yfs.h), sg2s=c(f$hv, yfn.v.h, yfs.v.h))
  
  yft.g[i] <- ft.g$ms
  yft.h[i] <- ft.h$ms
  
  vft.g[i] <- ft.g$vs
  vft.h[i] <- ft.h$vs
  
  save(yft.g, yft.h, vft.g, vft.h, file="inv-pred-f_v2.RData")
  print(i)
}

load("inv-pred-f_v2.RData")

## rmse  ## line 7 of Table 1
pm.inv <- as.matrix(data.frame(g=yft.g, h=yft.h))
rmse.inv <- apply(pm.inv, 2, rmse.cal, true=yf)
## rmse.inv
##     g       h        
## 25.09253 24.65131 

## score
pv.inv <- as.matrix(data.frame(g=vft.g, h=vft.h))
score.inv <- rep(NA, ncol(pm.inv))
for(i in 1:length(score.inv)){
    score.inv[i] <- score.cal(m=pm.inv[,i], v=pv.inv[,i], true=yf)
}
## score.inv
##     g       h
## 8.181672 8.558334

##### 95% predictive coverage (line 7 of Table 1: IVW.hat)
### global
cov95_ivw_g.per <- covper(true=yf, mean=yft.g, var=vft.g, perc=95)
# cov95_ivw_g.per
# 0.8390879
### local
cov95_ivw_l.per <- covper(true=yf, mean=yft.h, var=vft.h, perc=95)
# cov95_ivw_l.per
# 0.8156352

## calculate absolute error and p-values
## NAM + bias
### deterministic
load("ypred_F-N-nfM.RData") 
loo_nam_dt <- data.frame(g=yf.pred.g, h=yf.pred.h)
### unknown
load("ypred_ta_dis_ns.RData")  ## emulated discrepancy terms from "unknown" computer models
load("ypred_N-S.RData") ## unknown computer models
yN_dis_g <- ydis_N.pred.g + yg.N ## emulated NAM + emulated discrepancy: global
yN_dis_h <- ydis_N.pred.h + yh.N ## emulated NAM + emulated discrepancy: hybrid
loo_nam_un <- data.frame(ng=yN_dis_g, nh=yN_dis_h)
## IVW
load("inv-pred.RData")
loo_ivw_dt <- data.frame(g=yft.g, h=yft.h) ## deterministic computer model
load("inv-pred-f_v2.RData") ## unknown computer model
loo_ivw_un <- data.frame(g=yft.g, h=yft.h) ## unknown computer models
## calculate RMSE
rmse.ivw_un <- apply(loo_ivw_un, 2, rmse.cal, true=yf)
## rmse.ivw_un
##     g        h 
## 25.09253 24.65131

## calculate the absolute error
## computer model W/O bias
## NAM deterministic
nam_ob_dt.error <- apply(as.data.frame(yN), 2, function(x, y) abs(x-y), y=yf)
## SREF deterministic
sref_ob_dt.error <- apply(as.data.frame(yS), 2, function(x, y) abs(x-y), y=yf)
## NAM fitted
pm.NAM <- as.matrix(data.frame(g=yg.N, h=yh.N))
nam_ob_un.error <- apply(pm.NAM, 2, function(x, y) abs(x-y), y=yf)
## SREF fitted
pm.SREF <- as.matrix(data.frame(g=yg.S, h=yh.S))
sref_ob_un.error <- apply(pm.SREF, 2, function(x, y) abs(x-y), y=yf)

## NAM.fit W/ bias.fit (line 6 of Table 1)
rmse.nwb_f <- apply(loo_nam_un, 2, rmse.cal, true=yf)
## rmse.nwb_f
##    ng        nh 
## 25.30079  24.70228
nam_dt.error <- apply(loo_nam_dt, 2, function(x, y) abs(x-y), y=yf)
nam_un.error <- apply(loo_nam_un, 2, function(x, y) abs(x-y), y=yf)
ivw_dt.error <- apply(loo_ivw_dt, 2, function(x, y) abs(x-y), y=yf)
ivw_un.error <- apply(loo_ivw_un, 2, function(x, y) abs(x-y), y=yf)

### paired t-test
## load LOO-CV result for field
load("mlegp_lagp.RData")

######### emulated computer models
##### make a comparison across the methods
## NAM.fit w/o bias vs field.fit (line 5 of Table 1)
# global
t.test(log(nam_ob_un.error[,1]), log(field.error_m), paired=TRUE, alternative="less")
## 0.9004
# hybrid
t.test(log(nam_ob_un.error[,2]), log(field.error_h), paired=TRUE, alternative="less")
## 1

## NAM.fit w/ bias vs. NAM.fit w/o bias (line 6 of Table 1)
# global
t.test(log(nam_un.error[,1]), log(nam_ob_un.error[,1]), paired=TRUE, alternative="less")
# < 2.2e-16
# hybrid
t.test(log(nam_un.error[,2]), log(field.error_h), paired=TRUE, alternative="less")
# 0.2305

## IVW.fit vs NAM.fit w/ bias (line 7 of Table 1)
# global
t.test(log(ivw_un.error[,1]), log(nam_un.error[,1]), paired=TRUE, alternative="less")
# 0.05625
# hybrid
t.test(log(ivw_un.error[,2]), log(nam_un.error[,2]), paired=TRUE, alternative="less")
# 0.4069

## IVW.fit vs field.fit (for Furong's proposal defense's presentation---not in the table)
# global
t.test(log(ivw_un.error[,1]), log(field.error_m), paired=TRUE, alternative="less")
# < 2.2e-16

##### make a comparison within ONE method
## SREF.fit W/O bias (line 4 of Table 1)
t.test(log(sref_ob_un.error[,2]), log(sref_ob_un.error[,1]), paired=TRUE, alternative="less")
# 0.6605
## NAM.fit W/O bias (line 5 of Table 1)
t.test(log(nam_ob_un.error[,2]), log(nam_ob_un.error[,1]), paired=TRUE, alternative="less")
# 0.3857
## NAM.fit W/ bias (line 6 of Table 1)
t.test(log(nam_un.error[,2]), log(nam_un.error[,1]), paired=TRUE, alternative="less")
# 0.01624
## IVW.fit (line 7 of Table 1)
t.test(log(ivw_un.error[,2]), log(ivw_un.error[,1]), paired=TRUE, alternative="less")
# 0.09994


######### deterministic computer models
##### make a comparison across the methods
## NAM w/o bias vs. IVW.fit
t.test(log(nam_ob_dt.error[,1]), log(ivw_un.error[,1]), paired=TRUE, alternative="less")
# 1

## NAM w/ bias vs. IVW.fit (line 10 of Table 1)
## global
t.test(log(nam_dt.error[,1]), log(ivw_un.error[,1]), paired=TRUE, alternative="less")
# 0.01666
## hybrid
t.test(log(nam_dt.error[,2]), log(ivw_un.error[,2]), paired=TRUE, alternative="less")
# 0.06869

## IVW vs NAM w/ bias (line 11 of Table 1)
# global
t.test(log(ivw_dt.error[,1]), log(nam_dt.error[,1]), paired=TRUE, alternative="less")
# 0.5444
# hybrid
t.test(log(ivw_dt.error[,2]), log(nam_dt.error[,2]), paired=TRUE, alternative="less")
# 0.03503

##### make a comparison within a method: local (hybrid) vs. global
## NAM w/ bias (line 10 of Table 1)
t.test(log(nam_dt.error[,2]), log(nam_dt.error[,1]), paired=TRUE, alternative="less")
# 0.3811
## IVW (line 11 of Table 1)
t.test(log(ivw_dt.error[,2]), log(ivw_dt.error[,1]), paired=TRUE, alternative="less")
# 0.006098
