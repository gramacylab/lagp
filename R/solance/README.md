## This directory contains code supporting the project on predicting solar irradiance across the continental United States in collabration with University of Utah and IBM Thomas J. Watson Research Center; for details, see Sun, et.al., 2019 (Statistical Analysis and Data Mining)

The original dataset includes **THREE** solar irradiance outputs in GHI (*g*lobal *h*orizontal *i*rradiance): field measurements, NAM (the *N*orth *A*merican *M*esoscale Forecast System) and SREF (the *S*hort *R*ange *E*nsemble *F*orecast system) at *1535* spatial locations (defined by latitude & longitude) across the continental United States.

### The content of each file is listed as follows:  

- `mlegp_ta.R`: to make a comparison between *mlegp* and *laGP* regarding the out-of-sample prediction using the locations (defined by latitude & longitude) as the input space by applying Leave-one-out cross-validation (LOO-CV); to map the sparse locations defined in `grid_dy.R` to *on-grid* locations; compare *laGP* with *BART* in the context of 10-fold CV; contributes to part of **Table 1**.
  
- `loo_ta.R`: to emulate the systematic bias (i.e., discrepancy) between field measurements and computer model outputs using varied GP emulators while treating computer model either as unknown or known, and perform out-of-sample prediction in the context of LOO-CV.
  
- `ivw_ta.R`: to combine field, NAM (plus its discrepancy), and SREF (plus its discrepancy) using IVW to perform out-of-sample prediction in the context of LOO-CV; to compile results contributing to part of **Table 1**.

- `grid_ta.R`: to predict time-aggregated solar irradiance at the grid locations across the continental United States using IVW; **Figure 1** is generated. 

- `proc_dy.R`: to aggregate the hourly data into daily data in different ways; particularly, a simple linear regression model conditioned on the computer model outputs on the same day, the field measurements from the previous day as well as the periodic predictors is applied; **Figure 2** is generated.

- `loo_dy.R`: to make prediction in the context of LOO-CV using IVW for the daily data; **Table 2** and **Figure 3** are generated.

- `pear_dy.R`: to use the NAM output obtained from IBM PAIRS at $978$ (the first version; the second version is $9698$) space-filling grid locations to augment the original dataset; to use the same idea illustrated in `loo_dy.R` to combine the two computer model outputs and field measurements to understand how much improvment on the predictive accuracy if there are more computer model outputs --- even only one computer model output is publicly available; to compile results for **Table 3**.

- `grid_dy.R`: to generate **Figures 4 & 5**. Particularly, the movie showing daily solar irradiance within one whole year on $81470$ grid locations across the continental United States, which is the supplementary material of the solance paper, is generated; the new space-filling grid locations on which to obtain new NAM output is also generated (**Figure 6**). 

- `daily_time.R`: to compare the global subset GP and local GP on the daily solance irradiance data (NAM, SREF, and field) across the continental United States. The first-year data is used as a training set, while the remaining data (around half a year) is used as a testing set. The prediction is performed in the context of LOO-CV using IVW by treating the computer model outputs as known.
  
- `daily_time.qsub`: the bash script to submit 'daily_time.R' in the Advanced Research Computing of Virginia Tech

- `daily_compare.R`: Bobby's version of `loo_dy.R` and `pear_dy.R`.

- `function.R`: contains necessary functions related to the solance project.

- `rmkl.sh`: the bash script to run R files using R with Intel MKL (Rmkl) remotely with `nohup'.
