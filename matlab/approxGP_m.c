#include <assert.h>
#include <stdlib.h>
# ifdef _OPENMP
#include <omp.h>
# endif
#include "mex.h"
#include "matrix.h"
#include "gp.h"
#include "rhelp.h"

/* Input Arguments */
#define gpi_IN          prhs[0]
#define XX_IN           prhs[1]
#define end_IN          prhs[2]
#define X_IN            prhs[3]
#define Z_IN            prhs[4]
#define close_IN        prhs[5]
#define verb_IN         prhs[6]

/* Output Arguments */
#define mean_OUT        plhs[0]
#define var_OUT         plhs[1]
#define llik_OUT        plhs[2]

/* helper defines */
#define M(A) mxGetM(A)
#define N(A) mxGetN(A)

/*
 * Start MEX function
 */

extern unsigned int NGP;
extern struct gp** gps;

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray*prhs[] )

{
  GP* gp;
  unsigned int m, n, gpi, close, verb, nn, verb2, end;
  int i;
  double **X, **XX, *Z, *d, *g, *mean, *var, *llik;

  /* Check for proper number of arguments */
  if (nrhs != 7) {
    mexErrMsgTxt("Seven input arguments required.");
  } else if (nlhs > 3) {
    mexErrMsgTxt("Too many output arguments.");
  }

  /* get and check dims */
  m = M(X_IN);
  n = N(X_IN);
  nn = N(XX_IN);
  if(n != M(Z_IN)) mexErrMsgTxt("mismatch in X-Z lengths");
  if(m != M(XX_IN)) mexErrMsgTxt("mismatch in X-XX cols");
  
  /* get integers */
  close = (unsigned int) *(mxGetPr(close_IN));
  verb = (unsigned int) *(mxGetPr(verb_IN));
  end = (unsigned int) *(mxGetPr(end_IN));

  /* get the gp */
  gpi = (unsigned int) *(mxGetPr(gpi_IN));
  if(gps == NULL) mexErrMsgTxt("no gps allocated");
  if(gpi >= NGP) mexErrMsgTxt("not that many gps have been allocated");
  if(gps[gpi] == NULL) mexErrMsgTxt("that gp is not allocated\n");
  gp = gps[gpi];
  if(m != gp->m)  mexErrMsgTxt("ncol(X) does not match GP/C-side");

  /* Assign pointers to the various parameters */
  Z = mxGetPr(Z_IN);
  X = new_matrix_bones(mxGetPr(X_IN), n, m);
  XX = new_matrix_bones(mxGetPr(XX_IN), nn, m);

  /* copy verb */
#ifdef _OPENMP
  verb2 = verb = 0;
#else
  if(verb > 0) verb2 = verb - 1;
  else verb2 = 0;
#endif

  /* allocate outputs */
  gpi = get_gp();
  mean_OUT = mxCreateDoubleMatrix(nn,1,0);
  mean = mxGetPr(mean_OUT);
  var_OUT = mxCreateDoubleMatrix(nn,1,0);
  var = mxGetPr(var_OUT);
  llik_OUT = mxCreateDoubleMatrix(nn,1,0);
  llik = mxGetPr(llik_OUT);
  
  /* for each predictive location */
#ifdef _OPENMP
  #pragma omp parallel for private(i)
#endif
  for(i=0; i<nn; i++) {

    /* copy in predictive location; MAYBE move inside localGP */
    double ** Xref = new_matrix(1, m);
    dupv(*Xref, XX[i], m);

    /* make a copy of the gp */
    GP* temp_gp = copyGP(gp);

    /* progress meter - can't do within openmp */
    if(verb) {
      myprintf(mystdout, "i=%d (of %d), XX=", i+1, nn);
      printVector(XX[i], m, mystdout, HUMAN);
    }
    
    /* call C-only code */
    double df, s2;
    localGP(temp_gp, Xref, 1, end, n, X, Z, close, verb2, NULL, 
	    &mean[i], &s2, &df, &llik[i]);
    var[i] = df*s2/(df-2.0);

    /* clean up */
    delete_matrix(Xref);
    deleteGP(temp_gp);
  }

  /* clean up */
  free(X);
  free(XX);
  
  return;
}
