#include <assert.h>
#include <stdlib.h>
# ifdef _OPENMP
#include <omp.h>
# endif
#include "mex.h"
#include "matrix.h"
#include "rhelp.h"
#include "gp.h"

/* Input Arguments */
#define X_IN           prhs[0]
#define Z_IN           prhs[1]
#define d_IN           prhs[2]
#define g_IN           prhs[3]

/* Output Arguments */
#define gpi_OUT        plhs[0]

/* helper defines */
#define M(A) mxGetM(A)
#define N(A) mxGetN(A)

/*
 * Start MEX function
 */

extern unsigned int NGP;
extern struct gp** gps;

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray*prhs[] )

{
  unsigned int m, n, gpi;
  double **X, *Z, *d, *g;

  /* Check for proper number of arguments */
  if (nrhs != 4) {
    mexErrMsgTxt("Four input arguments required.");
  } else if (nlhs > 1) {
    mexErrMsgTxt("Too many output arguments.");
  }

  /* get and check dims */
  m = M(X_IN);
  n = N(X_IN);
  if(n != M(Z_IN)) mexErrMsgTxt("mismatch in X-Z lengths");

  /* Assign pointers to the various parameters */
  Z = mxGetPr(Z_IN);
  X = new_matrix_bones(mxGetPr(X_IN), n, m);
  d = mxGetPr(d_IN);
  g = mxGetPr(g_IN);

  /* get new GP */
  gpi = get_gp();
  gpi_OUT = mxCreateDoubleScalar((double) gpi);

  /* Do the actual computations in a sub-routine */
  gps[gpi] = newGP(m, n, X, Z, *d, *g);
  
  /* clean up */
  free(X);
  
  return;
}
