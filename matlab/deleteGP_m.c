#include <assert.h>
#include <stdlib.h>
# ifdef _OPENMP
#include <omp.h>
# endif
#include "mex.h"
#include "matrix.h"
#include "rhelp.h"
#include "gp.h"

/* Input Arguments */
#define gpi_IN         prhs[0]

/* helper defines */
#define M(A) mxGetM(A)
#define N(A) mxGetN(A)

/*
 * Start MEX function
 */

extern unsigned int NGP;
extern struct gp** gps;

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray*prhs[] )

{
  unsigned int gpi;

  /* Check for proper number of arguments */
  if (nrhs != 1) {
    mexErrMsgTxt("One input argument required.");
  } else if (nlhs > 0) {
    mexErrMsgTxt("Too many output arguments.");
  }

  /* get the index */
  gpi = (unsigned int) *(mxGetPr(gpi_IN));

  /* call C-side delete */
  if(!(gps == NULL || gpi >= NGP || gps[gpi] == NULL)) deleteGP_index(gpi);
  else mexErrMsgTxt("that gp index is not allocated\n");

  return;
}
