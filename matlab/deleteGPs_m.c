#include <assert.h>
#include <stdlib.h>
# ifdef _OPENMP
#include <omp.h>
# endif
#include "mex.h"
#include "matrix.h"
#include "rhelp.h"
#include "gp.h"

/* helper defines */
#define M(A) mxGetM(A)
#define N(A) mxGetN(A)

/*
 * Start MEX function
 */

extern unsigned int NGP;
extern struct gp** gps;

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray*prhs[] )

{
  unsigned int gpi;

  /* Check for proper number of arguments */
  if (nrhs != 0) {
    mexErrMsgTxt("No input arguments needed.");
  } else if (nlhs > 0) {
    mexErrMsgTxt("Too many output arguments.");
  }

  /* call C-side delete */
  deleteGPs();

  return;
}
