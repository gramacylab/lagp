## This directory contains datasets used in the *laGP* examples.

### A brief description of the contents of each subdirectory follows.

- `sattemp`: the datasets related to the satellite temperature project, with implementation in lagp/R/misc

- `crash`: data for the radiative shock hydrodynamics calibration project, with examples in lagp/R/calib

- `sarcos`: robot-arm benchmark data set from *Gaussian Processes for Machine Learning (GPML)* book by Williams and Rasmussen

- `simce.txt`: another set of examples from GPML book

- `lgbb_fill.RData`:  version of Langley Glide-back Booster data used in lagp/R/lgbb 

- `solance`: the datasets related to the solar irradiance project (lagp/R/solance).  This content has been moved to new repository `solance-hg/data`.  It will soon be removed.
    
    + lola.csv: the latitude & longitude information of 1535 spatial locations across the continental United States
    + sol_ta.csv: the time aggregated solar irradiance output for field, NAM, and SREF at the spatial locations describeed in `lola.csv` 
    + daily.csv: the daily time-incorporated solar irradiance output for field, NAM, and SREF, the yearDay, and the periodic information at the same locations of `sol_ta.csv`
    + lola_new.csv: the latitude & longitude information of 978 space-filling locations across the continental United States
    + nam_new.csv: the NAM output (obtained from IBM PAIRS; courtesy of Siyuan Lu) at the spatial locations in `lola_new.csv`
  
### This directory used to contain the following sub-directories which have been moved to new repositories.

- `tpm`: code and data for the test particle Monte Carlo estimation of satellite drag coefficients; now in `gramacylab/tpm`