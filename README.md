# laGP: Local Approximate Gaussian Processes #

This repository contains all code including R packages, test examples and data, as well as all documentation including academic papers and talks, for the laGP method.

### What is laGP? ###

* laGP is an R package providing approximate GP regression for large computer experiments and spatial datasets. The approximation is based on finding small local designs for prediction (independently) at particular inputs.  The advantages are computational (easily parallelized) and methodological (cheap nonstationary modeling capability)
* The software, which can be found on http://cran.r-project.org/web/packages/laGP/index.html, is licensed under the GNU Lesser Public License (LGPL), version 2 or later
* For details on installation and references to papers, see http://bobby.gramacy.com/r_packages/laGP

## Features of the software include: ##

* ALC, MSPE and NN-based local approximation, as well as EFI-based global heuristics
* local MLE/MAP inference for (isotropic and separable) lengthscales and nuggets
* OpenMP for approximation over a vast out-of-sample testing set
* GPU acceleration for local ALC subroutine evaluations
* SNOW/parallel-package cluster parallelization
* computer model calibration via optimization
* blackbox constrained optimization via augmented Lagrangians
* an interface to lower-level (full) GP inference and prediction

## Besides the software, what else is in the repository? ##

* Examples supporting academic publications that didn't make it into the R package documentation, vignette or a set of demos
* Data, particularly for the CRASH calibration problem (another dataset involving laGP application, `satdrag`, is in the `tpm` repository)
* An early and crude (now deprecated) matlab interface to the underlying laGP C routines

### Who do I talk to? ###

* The project was originated and is actively maintained by Robert B. Gramacy <rbg@vt.edu>
* Contributors include Dan Apley, Jarad Niemi, Robin Weiss, Derek Bingham, Ben Haaland, Chih-Li Sung, Earl Lawrence, Andrew Walker, Furong Sun, Adam Edwards

### Forget me not ###

* Adam's idea to remove initial NN design after a few ALC iterations and then entertain adding back in.  This could especially help with set-based (path-based) ALC
* `laGPsep` is not using new mle `"both"` implementation
* `aGP` and `aGPsep` does not offer `"alcopt"` alternative, which could be especially useful in the separable case 
* note that pointwise alternatives to joint laGP are (effectively) using much more local design points
